# TimeBomb #

One of my first plugins. Buggy and not up to date.

This plugin once got got featured on the [Minecraft forums](http://www.minecraftforum.net/topic/1350790-community-creations-timebomb-taco-bell/)! It was awesome and new at that time...

If you are updating this plugin or improving it, please send a pull request! Sharing is caring.


### License ###

The TimeBomb plugin (and its source code) by ftbastler are licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/) (CC BY-SA 4.0).