package events;

import java.util.Random;

import main.Game;
import main.TimeBomb;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Wool;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;
import org.kitteh.tag.PlayerReceiveNameTagEvent;

import utilities.Cooldown;
import utilities.Freeze;
import utilities.ItemNamer;
import utilities.ParticleEffect;
import utilities.Points;
import utilities.enums.GameState;
import utilities.enums.GameType;

public class GameEvent implements Listener {
	
	public static Boolean give_msg;
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {	
		if(event.getEntity() instanceof Player && event.getDamager() instanceof Player && Game.state == GameState.PROGRESS) {
			Player p = (Player) event.getEntity();
			Player d = (Player) event.getDamager(); 
			
			if(!Game.players.contains(d) || !Game.players.contains(p))
				return;
			
			event.setCancelled(true);
			if(Game.tnt.contains(d) && !Game.tnt.contains(p) && !Game.gold.contains(p)) {
				if(Cooldown.isCool(d)) {
					d.sendMessage("�7Cooldown! Watch your XP bar.");
					return;
				} else {
					p.getInventory().setItem(8, ItemNamer.gameitems(new ItemStack(Material.TNT, 1)));
					
					if(give_msg)
						if(Game.type == GameType.CLASSIC)
							Game.messagePlayers("�b" + d.getName() + "�b gave a TimeBomb to " + p.getName());
						else if(Game.type == GameType.INFECTED)
							Game.messagePlayers("�b" + d.getName() + "�b infected " + p.getName() + "�b with a TimeBomb");
					
					p.playSound(p.getLocation(), Sound.FUSE, 1, 1);
					d.playSound(d.getLocation(), Sound.WITHER_SHOOT, 1, 1);
					
					ParticleEffect.LAVA.play(p.getLocation().add(0, 2, 0), 0, 0, 0, 1, 5);
					
					Game.tnt.add(p);
					Game.setNameTag(p, ChatColor.RED);
					
					Cooldown.addCool(p);
					d.setExp(0);
					
					if(Game.type == GameType.CLASSIC) {
						d.getInventory().removeItem(new ItemStack(Material.TNT));
						d.getInventory().setItem(8, null);
						ParticleEffect.playTileCrack(d.getLocation().add(0, 1, 0), 46, (byte) 0, 0, 0, 0, 50);
						Game.tnt.remove(d);
						Game.setNameTag(d, null);
					} else if(Game.type == GameType.INFECTED) {
						Cooldown.addCool(d);
						Game.checkEndGame();
						Points.addPoints(d, "You tagged somebody!", 1, 3);
						Points.subPoints(p, "You got tagged!", 3);
					}
					
					p.updateInventory();
					d.updateInventory();
				}
			}
			
			if(Game.gold.contains(p) && !Game.tnt.contains(d)) {
				p.getInventory().removeItem(new ItemStack(Material.GOLD_BLOCK));
				p.getInventory().setItem(8, null);
				d.getInventory().setItem(8, ItemNamer.gameitems(new ItemStack(Material.GOLD_BLOCK, 1)));
				p.updateInventory();
				d.updateInventory();
				
				if(give_msg)
					Game.messagePlayers("�b" + d.getName() + "�b grabbed the GoldProtection from " + p.getName());
				
				d.playSound(d.getLocation(), Sound.ORB_PICKUP, 1, 1);
				p.playSound(p.getLocation(), Sound.IRONGOLEM_HIT, 1, 1);
				
				if(p.hasPotionEffect(PotionEffectType.SPEED))
					p.removePotionEffect(PotionEffectType.SPEED);
				
				if(p.hasPotionEffect(PotionEffectType.JUMP))
					p.removePotionEffect(PotionEffectType.JUMP);
				
				d.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1200, 1));
				d.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 1200, 1));
				
				try {
					ParticleEffect.FIREWORKS_SPARK.play(d.getLocation(), 0, 0, 0, 1, 5);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				try {
					ParticleEffect.ANGRY_VILLAGER.play(p.getLocation(), 0, 0, 0, 1, 5);
					ParticleEffect.playTileCrack(p.getLocation().add(0, 1, 0), 41, (byte) 0, 0, 0, 0, 50);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				Game.gold.add(d);
				Game.gold.remove(p);
				Game.setNameTag(d, ChatColor.GOLD);
				Game.setNameTag(p, null);
			}
		}
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		Player p = event.getPlayer();
		
		if(!Game.players.contains(p))
			return;
		
		Block b = p.getLocation().getBlock().getRelative(BlockFace.DOWN);	
		if(b.getType() == Material.WOOL && Game.gamers.contains(p)) {
			Wool w = (Wool) b.getState().getData();
			if(Game.state == GameState.PROGRESS) {
				if(w.getColor() == DyeColor.BLUE) {
					Game.plspawn.getWorld().playEffect(p.getLocation(), Effect.MOBSPAWNER_FLAMES, 1);
					p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 8 * 20, 1));
					p.playSound(p.getLocation(), Sound.FIRE_IGNITE, 1, 1);
				}
				if(w.getColor() == DyeColor.RED) {
					Game.plspawn.getWorld().playEffect(p.getLocation(), Effect.MOBSPAWNER_FLAMES, 1);
					p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 8 * 20, 1));
					p.playSound(p.getLocation(), Sound.FIRE_IGNITE, 1, 1);
				}
				if(w.getColor() == DyeColor.GREEN) {
					Game.plspawn.getWorld().playEffect(p.getLocation(), Effect.MOBSPAWNER_FLAMES, 1);
					p.setVelocity(new Vector(0, 1, 0));
					p.playSound(p.getLocation(), Sound.FIZZ, 1, 1);
				}
			}
		}
	}	
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(Game.state == GameState.PROGRESS) {
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
			Player p = event.getPlayer();
			
			if(!Game.players.contains(p))
				return;
			
			Material material = event.getPlayer().getItemInHand().getType();
			Integer m = 0;
			Integer affectedentities = 3;
			 if(material == Material.SLIME_BALL)
		    	  m = 1; //Slimeball
			 else if(material == Material.SNOW_BALL)
		    	  m = 2; //Snowball
			 
			if(m > 0 && m < 3) {
				Location ploc = p.getEyeLocation();
			      ItemStack is2 = new ItemStack(p.getItemInHand());
			      is2.setAmount(1);
			      Item grenade = p.getWorld().dropItem(ploc, is2);
			      ItemStack is = p.getItemInHand();
			      int am = is.getAmount();
			      if (am > 0)
			        is.setAmount(am - 1);
			      else
			        is = null;
			      p.setItemInHand(is);
			      p.updateInventory();
			      grenade.setVelocity(ploc.getDirection());
			      grenade.setPickupDelay(1200);
			      
			      final Player pe = p;
			      final Item ball = grenade;
			      final Integer mat = m;
			     final Double room = Double.parseDouble(affectedentities + "");
			      
			      final Integer task = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(TimeBomb.instance, new Runnable() {
			    	  public void run() {
			    		  Game.plspawn.getWorld().playEffect(ball.getLocation(), Effect.SMOKE, BlockFace.UP);
			    		  Game.plspawn.getWorld().playEffect(ball.getLocation(), Effect.SMOKE, BlockFace.UP);
			    		  Game.plspawn.getWorld().playEffect(ball.getLocation(), Effect.SMOKE, BlockFace.UP);
			    	  }
			      },  10, 10);
			      
			      
			          Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(TimeBomb.instance, new Runnable() {
			            public void run() {
			            	if(mat == 1)
			            		Game.plspawn.getWorld().playEffect(ball.getLocation(), Effect.POTION_BREAK, 12);
			            	if(mat == 2)
			            		Game.plspawn.getWorld().playEffect(ball.getLocation(), Effect.POTION_BREAK, 1);
			            	
			            	for (Entity e : ball.getNearbyEntities(room, room, room)) {
								if ((e instanceof Player)) {
									Player pl = (Player) e;
									if (pl.getName() != pe.getName() && Game.gamers.contains(pl)) {
										
										
										if(mat == 1) {
											Random r = new Random();
											pl.setVelocity(new Vector(1 - r.nextInt(3), r.nextInt(2), 1 - r.nextInt(3)));
											ParticleEffect.MOB_SPELL_AMBIENT.play(ball.getLocation(), 16, 1, 1, 1, 0, 50);
										}
										
										if(mat == 2) {
											Random r = new Random();
											switch (r.nextInt(2)) {
											case 0:
												pl.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 160, 1));
												break;
											case 1:
												pl.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 100, 1));
												break;
											default:
												break;
											}
											ParticleEffect.MOB_SPELL_AMBIENT.play(ball.getLocation(), 16, 1, 1, 1, 0, 50);
											pl.playSound(pl.getLocation(), Sound.AMBIENCE_CAVE, 1, 1);
											pl.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 100, 1));
										}										
									}
								}
			            	}
			            	Bukkit.getServer().getScheduler().cancelTask(task);
			            	ball.remove();
			            }
			          } , 25);
			} else if(material == Material.FEATHER) {
				  Location ploc = p.getEyeLocation();
				  
			      ItemStack is = p.getItemInHand();
			      int am = is.getAmount();
			      if (am > 0)
			        is.setAmount(am - 1);
			      else
			        is = null;
			      p.setItemInHand(is);
			      p.updateInventory();
			      
			      p.setVelocity(ploc.getDirection().multiply(2));
			      p.playSound(p.getLocation(), Sound.FIZZ, 1, 1);
			}
		}
	}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void PlayerInteractEntity(PlayerInteractEntityEvent event) {
		if(Game.state == GameState.PROGRESS) {
		if(event.getPlayer().getItemInHand().getType() == Material.STICK && event.getRightClicked() instanceof Player) {
			Player p = event.getPlayer();
		    Player d = (Player) event.getRightClicked();
			
			if(!Game.gamers.contains(p) && !Game.gamers.contains(d))
				return;
			
			if(Freeze.isFreezed(d)) {
				p.sendMessage("�cPlayer is already freezed.");
				return;
			}
			
		      ItemStack is = p.getItemInHand();
		      int am = is.getAmount();
		      if (am > 0)
		        is.setAmount(am - 1);
		      else
		        is = null;
		      p.setItemInHand(is);
		      p.updateInventory();
		      
		      Freeze.addFreezed(d);
		      p.sendMessage("�aPlayer freezed for 4 seconds!");
		}
		}
	}
	
    @EventHandler
    public void onNameTag(PlayerReceiveNameTagEvent event) {
    	if(!Game.players.contains(event.getNamedPlayer()))
    		return;
    	
    	if (Game.tnt.contains(event.getNamedPlayer())) {
    		event.setTag("�c" + event.getNamedPlayer().getName());
    		return;
    	}

    	if (Game.gold.contains(event.getNamedPlayer())) {
    		event.setTag("�6" + event.getNamedPlayer().getName());
    		return;
    	}
    	
    	event.setTag(event.getNamedPlayer().getName());
    }
	
}
