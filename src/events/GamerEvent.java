package events;


import java.util.Random;

import main.Game;
import main.TimeBomb;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.util.Vector;

import timers.SignTimer;
import utilities.Freeze;
import utilities.InfoBook;
import utilities.MySQL;
import utilities.Permissions;
import utilities.Points;
import utilities.Updater;
import utilities.enums.GameState;

public class GamerEvent implements Listener {
			
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		if(Game.freeze && event.getFrom() != event.getTo() && Game.gamers.contains(event.getPlayer())) {
			event.setCancelled(true);
			event.getPlayer().teleport(event.getFrom());
		}
		if(Game.state == GameState.PROGRESS && event.getFrom() != event.getTo() && Freeze.isFreezed(event.getPlayer())) {
			event.setCancelled(true);
			event.getPlayer().teleport(event.getFrom());
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player p = event.getPlayer();

		if(!MySQL.playerPlayedBefore(p.getName()))
			MySQL.query("INSERT INTO `tb_PLAYERS` (`NAME`, `PLAYED`, `DIED`, `POINTS`, `CASH`) VALUES ('" + p.getName() + "', 0, 0, 0, 15)");
		
		if(Permissions.isAdmin(p)) {
			Updater updater = new Updater();
			if(updater.checkForUpdates())
				p.sendMessage(ChatColor.GREEN + "New version available: �r" + updater.getNewVersionString());
			else
				p.sendMessage(ChatColor.GREEN + "No updates have been found.");
		}
		
		if(Game.players.contains(p)) {
			p.setExp(0);
			p.setLevel(0);
			p.setHealth(20.0);
			p.setFoodLevel(20);
			p.getInventory().clear();
			p.getInventory().setHelmet(null);
			p.getInventory().setChestplate(null);
			p.getInventory().setLeggings(null);
			p.getInventory().setBoots(null);
			Points.setArmor(p);
		
			final Player player  = p;
			Bukkit.getScheduler().scheduleSyncDelayedTask(TimeBomb.instance, new Runnable() {
				public void run() {
					player.teleport(TimeBomb.lobby);
				}
			}, 10);
			return;
		} 
		
		if(TimeBomb.autolobby) {
			final Player player  = p;
			Bukkit.getScheduler().scheduleSyncDelayedTask(TimeBomb.instance, new Runnable() {
				public void run() {
					Game.join(player);
				}
			}, 10);
			return;
		}
	}
		
	@EventHandler
	public void onPlayerCommand(PlayerCommandPreprocessEvent event) {
		if(event.isCancelled())
			return;
		
		if(Game.players.contains(event.getPlayer())) {
			event.setCancelled(true);
			
			String[] cmds = {"/tb","/tbzone","/tbshop",
							"/list","/msg","/r",
							"/ban","/kick","/tempban","/mute","/say","/broadcast"};
			
			for(String s : cmds) {
				if(event.getMessage().toLowerCase().startsWith(s)) {
					event.setCancelled(false);
					break;
				}
			}
			
			if(event.isCancelled())
				event.getPlayer().sendMessage("�cYou have to leave TimeBomb first. �r/tb leave");
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player p = event.getPlayer();		
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(event.isCancelled())
				return;
			if(event.getClickedBlock().getType() == Material.WALL_SIGN || event.getClickedBlock().getType() == Material.SIGN || event.getClickedBlock().getType() == Material.SIGN_POST) {
				final Block block = event.getClickedBlock();
				final Sign sign = (Sign) block.getState();				
				if(sign.getLine(0).equalsIgnoreCase("[tb stats]")) {
					sign.setLine(1, "�b" + p.getName());
					sign.setLine(2, "Deaths: " + MySQL.getDeaths(p.getName()) + "/" + MySQL.getPlayedGames(p.getName()));
					sign.setLine(3, "Score: " + MySQL.getPoints(p.getName()) + " | " + MySQL.getCash(p.getName()) + "$");
					sign.update();
					Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(TimeBomb.getInstance(), new Runnable() {
						@Override
						public void run() {
							sign.setLine(1, "");
							sign.setLine(2, "�oCLICK");
							sign.setLine(3, "�oHERE");
							sign.update();
						}
					}, 20*10);
				} else if(sign.getLine(0).equalsIgnoreCase("[tb join]")) {
					Game.join(p);
				} else if(sign.getLine(0).equalsIgnoreCase("[tb leave]")) {
					Game.leave(p, false);
				}  else if(sign.getLine(0).equalsIgnoreCase("[tb shop]")) {
					p.performCommand("tbshop");
				} else if(sign.getLine(0).equalsIgnoreCase("[tb score]")) {
					p.performCommand("tbscore");
				} else if(sign.getLine(0).equalsIgnoreCase("[tb help]")) {
					p.getInventory().clear();
					p.getInventory().addItem(InfoBook.getBook());
					p.updateInventory();
					p.sendMessage("�aThis book should help you. :)");
				}
			}
		}
		
		//if(!Permissions.isAdmin(event.getPlayer()))
			//event.setCancelled(true);
	}
		
	@EventHandler
	public void onSignChange(SignChangeEvent event) {
		if(Permissions.isAdmin(event.getPlayer())) {
			if(event.isCancelled())
				return;
			
			if(event.getLines()[0].equalsIgnoreCase("[tb info]")) {
				Block block = event.getBlock();
				Sign sign = (Sign) event.getBlock().getState();
				ConfigurationSection cs = TimeBomb.instance.getConfig().getConfigurationSection("signs");
				String lkey = null;
				for(String key : cs.getKeys(false))
					lkey = key;
				
				if(lkey == null) lkey = "0";
				ConfigurationSection c  = cs.createSection((Integer.parseInt(lkey) + 1) + "");
				c.set("X", block.getX());
				c.set("Y", block.getY());
				c.set("Z", block.getZ());
				c.set("W", block.getWorld().getName());
				TimeBomb.instance.saveConfig();
				    
				SignTimer.addSign(sign);
				event.getPlayer().sendMessage("�aCreated TimeBomb information sign.");
			} else if(event.getLines()[0].equalsIgnoreCase("[tb join]")) {
				event.setLine(1, "");
				event.setLine(2, "�aJoin");
				event.setLine(3, "�aTimeBomb");
				event.getPlayer().sendMessage("�aCreated TimeBomb join sign.");
			} else if(event.getLines()[0].equalsIgnoreCase("[tb leave]")) {
				event.setLine(1, "");
				event.setLine(2, "�cLeave");
				event.setLine(3, "�cTimeBomb");
				event.getPlayer().sendMessage("�aCreated TimeBomb leave sign.");
			} else if(event.getLines()[0].equalsIgnoreCase("[tb score]")) {
				event.setLine(1, "");
				event.setLine(2, "�eView");
				event.setLine(3, "�escore");
				event.getPlayer().sendMessage("�aCreated TimeBomb score sign.");
			} else if(event.getLines()[0].equalsIgnoreCase("[tb shop]")) {
				event.setLine(1, "");
				event.setLine(2, "�eView");
				event.setLine(3, "�eshop");
				event.getPlayer().sendMessage("�aCreated TimeBomb shop sign.");
			} else if(event.getLines()[0].equalsIgnoreCase("[tb stats]")) {
				event.setLine(1, "");
				event.setLine(2, "�oCLICK");
				event.setLine(3, "�oHERE");
				event.getPlayer().sendMessage("�aCreated TimeBomb stats sign.");
			} else if(event.getLines()[0].equalsIgnoreCase("[tb help]")) {
				event.setLine(1, "");
				event.setLine(2, "�eView");
				event.setLine(3, "�ehelp");
				event.getPlayer().sendMessage("�aCreated TimeBomb help sign.");
			}
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		if(Game.players.contains(event.getPlayer())) {
			event.setCancelled(true);
			return;
		}
		
		if(event.getBlock().getType() == Material.WALL_SIGN || 
				event.getBlock().getType() == Material.SIGN || 
				event.getBlock().getType() == Material.SIGN_POST && 
				!event.isCancelled()) {
			Block block = event.getBlock();
			Sign sign = (Sign) block.getState();
			
			if(SignTimer.isSign(sign)) {
				event.getPlayer().sendMessage("�aIsSign!");
				if(!Permissions.isAdmin(event.getPlayer())) {
					event.getPlayer().sendMessage("�cYou can't destroy this sign!");
					event.setCancelled(true);
					return;
				}
				SignTimer.remSign(sign);
				ConfigurationSection cs = TimeBomb.instance.getConfig().getConfigurationSection("signs");
				for(String key : cs.getKeys(false)) {
					if(key == null || key == "")  continue;
					
					//TODO: Figure out why it doesn't work everytime
					ConfigurationSection c = cs.getConfigurationSection(key);
					if(block.getX() == c.getInt("X") &&
						block.getY() == c.getInt("Y") &&
						block.getZ() == c.getInt("Z") &&
						block.getWorld().getName() == c.getString("W")) {
						cs.set(key, null);
						TimeBomb.instance.saveConfig();
						event.getPlayer().sendMessage("�aDestroyed TimeBomb information sign.");
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		if(Game.players.contains(event.getPlayer()))
			event.setCancelled(true);
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {	
		if(!(event.getDamager() instanceof Player) && event.getEntity() instanceof Player) {
			if(!Game.players.contains((Player) event.getEntity()))
				return;
			event.setCancelled(true);
		}
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onEntityDamage(EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
		if(!Game.players.contains((Player) event.getEntity()))
			return;
				
		Player p = (Player) event.getEntity();
		if(event.getCause() == DamageCause.VOID) {
			if(Game.gamers.contains(p))
				if(Game.tnt.contains(p))
					p.teleport(Game.tbspawn);
				else
					p.teleport(Game.plspawn);
			else
				p.teleport(TimeBomb.lobby);
			event.setCancelled(true);
		}
		if(event.getCause() != DamageCause.ENTITY_ATTACK) {
			event.setCancelled(true);
		}
		if(event.getCause() == DamageCause.ENTITY_ATTACK && Game.state == GameState.NONE && !TimeBomb.lobbypvp) {
			event.setCancelled(true);
		}
		}
	}
	
	@EventHandler
	public void onPlayerGameModeChange(PlayerGameModeChangeEvent event) {
		if(!Game.players.contains(event.getPlayer()))
			return;
		
		if(Game.gamers.contains(event.getPlayer()) && event.getNewGameMode() == GameMode.CREATIVE) {
			Game.gamers.remove(event.getPlayer());
			event.getPlayer().sendMessage("�cYou were removed from the game!");
			event.getPlayer().teleport(TimeBomb.lobby);
			Game.checkEndGame();
		}
	}
	
	@EventHandler
	public void onPlayerTeleport(PlayerTeleportEvent event) {
		if(!Game.players.contains(event.getPlayer()))
			return;
		
		if(event.getCause() == TeleportCause.END_PORTAL || event.getCause() == TeleportCause.NETHER_PORTAL)
			event.setCancelled(true);
		if(event.getCause() == TeleportCause.ENDER_PEARL)
			event.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerKick(PlayerKickEvent event) {
		Player p = event.getPlayer();
		if(!Game.players.contains(p))
			return;
		
		Game.leave(p, true);
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		if(!Game.players.contains(event.getEntity()))
			return;
		
		event.setDroppedExp(0);
		event.getDrops().clear();
		event.setDeathMessage(null);
		event.getEntity().setExp(0);
		event.getEntity().setLevel(0);
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player p = event.getPlayer();
		if(!Game.players.contains(p))
			return;

		Game.leave(p, true);
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		Player p = event.getPlayer();
		if(Game.players.contains(p)) {
			if(Game.gamers.contains(p)) {
				event.setRespawnLocation(Game.plspawn);
			} else {
				event.setRespawnLocation(TimeBomb.lobby);
			}
			
			Random r = new Random();
			p.setVelocity(new Vector(1 - r.nextInt(3), 0.5, 1 - r.nextInt(3)));
			Points.setArmor(p);
		}
	}
	
	@EventHandler
    public void onClickInventory(InventoryClickEvent event) {
		if(Game.players.contains((Player) event.getWhoClicked()))
			event.setCancelled(true);
    }
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onProjectileLaunch(ProjectileLaunchEvent event) {
		if(event.getEntity().getShooter() instanceof Player) {
			if(Game.players.contains(event.getEntity().getShooter()))
				event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerBedEnter(PlayerBedEnterEvent event) {
		if(Game.players.contains(event.getPlayer()))
			event.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerShearEntity(PlayerShearEntityEvent event) {
		if(Game.players.contains(event.getPlayer()))
			event.setCancelled(true);
	}
			
	@EventHandler
	public void onItemDrop(PlayerDropItemEvent event) {
		if(Game.players.contains(event.getPlayer()))
			event.setCancelled(true);
	}
	
	@EventHandler
	public void onItemPickup(PlayerPickupItemEvent event) {
		if(Game.players.contains(event.getPlayer())) {
			event.setCancelled(true);
			event.getItem().remove();
		}
	}
	
	@EventHandler
	public void onBukketFill(PlayerBucketFillEvent event) {
		if(Game.players.contains(event.getPlayer()))
			event.setCancelled(true);
	}
	
	@EventHandler
	public void onBukketEmpty(PlayerBucketEmptyEvent event) {
		if(Game.players.contains(event.getPlayer()))
			event.setCancelled(true);
	}
	
	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		if(event.getEntity() instanceof Player) {
			if(Game.players.contains(event.getEntity()))
				event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onEntityRegainHealth(EntityRegainHealthEvent event) {
		if(event.getEntity() instanceof Player) {
			if(Game.players.contains(event.getEntity()))
				event.setCancelled(true);
		}
	}
}
