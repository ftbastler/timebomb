package utilities;

import main.TimeBomb;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Points {

	public static void setArmor(Player p) {
		try {
			Integer deaths = MySQL.getDeaths(p.getName());
			Integer games = MySQL.getPlayedGames(p.getName());
			Integer wins = games - deaths;
			int winrate = 0;
			if (games != 0) {
				winrate = (wins * 100) / games;
				if (winrate <= 65) {
					p.getInventory().setBoots(ItemNamer.armor(new ItemStack(Material.LEATHER_BOOTS)));
				} else if (winrate <= 75) {
					p.getInventory().setBoots(ItemNamer.armor(new ItemStack(Material.IRON_BOOTS)));
				} else if (winrate <= 85) {
					p.getInventory().setBoots(ItemNamer.armor(new ItemStack(Material.CHAINMAIL_BOOTS)));
				} else if (winrate <= 95) {
					p.getInventory().setBoots(ItemNamer.armor(new ItemStack(Material.GOLD_BOOTS)));
				} else if (winrate <= 100) {
					p.getInventory().setBoots(ItemNamer.armor(new ItemStack(Material.DIAMOND_BOOTS)));
				}
			}
		} catch(NullPointerException exception) {
			exception.printStackTrace();
			TimeBomb.getInstance().getLogger().warning("COULD NOT ESTABLISH MYSQL CONNECTION! PLUGIN WILL NOT WORK!");
		}
	}

	public static void subPoints(Player p, String msg, Integer pts, Integer cash) {
		Integer points = MySQL.getPoints(p.getName());
		if (points < pts) {
			pts = points;
		}
		Integer csh = MySQL.getCash(p.getName());
		if (csh < cash) {
			cash = csh;
		}
		MySQL.query("UPDATE `tb_PLAYERS` SET `POINTS` = `POINTS` - " + pts
				+ " WHERE `NAME` = '" + p.getName() + "';");
		MySQL.query("UPDATE `tb_PLAYERS` SET `CASH` = `CASH` - " + cash
				+ " WHERE `NAME` = '" + p.getName() + "';");
		if (msg != null && msg != "")
			p.sendMessage("�c" + msg + " Score: -" + pts + " points | -" + cash + " $");
	}
	
	public static void subPoints(Player p, String msg, Integer pts) {
		Integer points = MySQL.getPoints(p.getName());
		if (points < pts) {
			pts = points;
		}
		MySQL.query("UPDATE `tb_PLAYERS` SET `POINTS` = `POINTS` - " + pts
				+ " WHERE `NAME` = '" + p.getName() + "';");
		if (msg != null && msg != "")
			p.sendMessage("�c" + msg + " Score: -" + pts + " points");
	}

	public static void addPoints(Player p, String msg, Integer pts, Integer cash) {
		MySQL.query("UPDATE `tb_PLAYERS` SET `POINTS` = `POINTS` + " + pts
				+ " WHERE `NAME` = '" + p.getName() + "';");
		MySQL.query("UPDATE `tb_PLAYERS` SET `CASH` = `CASH` + " + cash
				+ " WHERE `NAME` = '" + p.getName() + "';");
		if (msg != null && msg != "")
			p.sendMessage("�a" + msg + " Score: +" + pts + " points | +" + cash + " $");
	}
	
	public static void addPoints(Player p, String msg, Integer pts) {
		MySQL.query("UPDATE `tb_PLAYERS` SET `POINTS` = `POINTS` + " + pts
				+ " WHERE `NAME` = '" + p.getName() + "';");
		if (msg != null && msg != "")
			p.sendMessage("�a" + msg + " Score: +" + pts + " points");
	}

}
