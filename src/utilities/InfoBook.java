package utilities;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

public class InfoBook {

	public static ItemStack getBook() {
		ItemStack book = new ItemStack(Material.BOOK_AND_QUILL, 1);
		BookMeta bm = (BookMeta) book.getItemMeta();
		
		ArrayList<String> content = new ArrayList<String>();
		content.add("�cTimeBomb \n\n�4The most explosive tag game!");
		content.add("�cGeneral \n�0One game lasts 60 seconds. \nRed wool gives you high jump, green wool a super high jump and blue wool speed. \nBoots indicate your lose/win ratio.");
		content.add("�cClassic game \n�0It is basically like tag. If you get tagged, you will be given the \"TimeBomb\". Wait for your XP bar to refill (cooldown). When it is full, tag another player by punching him.");
		content.add("�0After 60sec (XP level) all current holders of TimeBombs will explode and loose that game.");
		content.add("�0Sometimes a player spawns with the \"GoldProtection\". This player can't be tagged and got speed. If you are not holding a TimeBomb, you can steal the GoldProtection by punching that player.");
		content.add("�cInfection game \n�0In this gamemode, there are two teams. The TimeBomb team (infected) and the normal players (uninfected).");
		content.add("�0The goal for the infected team is to infect everybody. The uninfected players have to try not to get infected. No GoldProtection in this game.");
		bm.setAuthor("TimeBomb");
		bm.setTitle("Beginner's Guideline");
		bm.setPages(content);
		book.setItemMeta(bm);
		return book;
	}
	
}
