package utilities;

import java.util.ArrayList;

import main.Game;
import main.TimeBomb;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class Freeze {

	public static ArrayList<Player> freezedplayers = new ArrayList<Player>();
		
	public static boolean isFreezed(Player p) {
		return freezedplayers.contains(p);
	}
	
	public static void addFreezed(final Player p) {
		p.sendMessage("�6>>> �eYou got frozen for 4 seconds...");
		freezedplayers.add(p);
		
		p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 4*20, 2));
		p.getInventory().setHelmet(new ItemStack(Material.ICE, 1));
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(TimeBomb.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				removeFreezed(p);
			}
			
		}, 20*4);
	}
	
	public static void removeFreezed(Player p) {
		if(freezedplayers.contains(p)) {
			p.sendMessage("�6>>> �eYou are now unfrozen again.");
			freezedplayers.remove(p);
			
			if(Game.tnt.contains(p))
				p.getInventory().setHelmet(Game.getColoredArmor(new ItemStack(Material.LEATHER_HELMET, 1), Color.RED));
			else if(Game.gold.contains(p))
				p.getInventory().setHelmet(Game.getColoredArmor(new ItemStack(Material.LEATHER_HELMET, 1), Color.YELLOW));
			else
				p.getInventory().setHelmet(null);
		}
	}
	
	public static void clear() {
		freezedplayers.clear();
	}
	
}
