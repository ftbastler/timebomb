package utilities;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Permissions {
	
	public static boolean hasPerm(Player p, String s) {
		if(p.isOp())
			return true;
		return p.hasPermission(s);
	}
	
	public static boolean isVIP(Player p) {
		return Permissions.hasPerm(p, "tb.vip");
	}
	
	public static boolean isAdmin(Player p) {
		return Permissions.hasPerm(p, "tb.admin");
	}
	
	public static Player[] getOnlineAdmins() {
		ArrayList<Player> admins = new ArrayList<Player>();
		for (Player p : Bukkit.getServer().getOnlinePlayers()) {
			if (isAdmin(p))
				admins.add(p);
		}
		return (Player[]) admins.toArray(new Player[0]);
	}
}
