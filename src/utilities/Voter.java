package utilities;

import utilities.enums.VoteType;

public class Voter {

	public static void vote(String map, VoteType type) {
		map = map.toLowerCase();
		addMySQLMap(map);
		
		if(type == VoteType.UPVOTE) {
			MySQL.query("UPDATE `tb_MAPVOTES` SET `UPVOTES` = `UPVOTES` + " + 1 + " WHERE `NAME` = '" + map + "';");
		} else if(type == VoteType.DOWNVOTE) {
			MySQL.query("UPDATE `tb_MAPVOTES` SET `DOWNVOTES` = `DOWNVOTES` + " + 1 + " WHERE `NAME` = '" + map + "';");
		} else {
			return;
		}
	}
	
	public static void addMySQLMap(String map) {
		if(MySQL.mapExists(map))
			return;
		
		MySQL.query("INSERT INTO `tb_MAPVOTES`(`NAME`,`UPVOTES`,`DOWNVOTES`) VALUES ('" + map + "',0,0)");
	}	
}
