package utilities;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemNamer {

	public static ItemStack powerup(ItemStack itemstack) {
		Material type = itemstack.getType();
		if(type == Material.SNOW_BALL) {
			itemstack = setName(itemstack, "�r�aSnowball Grenade");
			itemstack = setLore(itemstack, "�r�9Right-click to throw.", "�oCauses confusion and blindess.");
		} else if(type == Material.SLIME_BALL) {
			itemstack = setName(itemstack, "�r�aSlimeBall Grenade");
			itemstack = setLore(itemstack, "�r�9Right-click to throw.", "�oMakes players fly.");
		} else if(type == Material.FEATHER) {
			itemstack = setName(itemstack, "�r�aMagic Feather");
			itemstack = setLore(itemstack, "�r�9Right-click to use.", "�oGives you a boost.");
		} else if(type == Material.STICK) {
			itemstack = setName(itemstack, "�r�aMagic Stick");
			itemstack = setLore(itemstack, "�r�9Right-click a player to use.", "�oFreezes a player for some seconds.");
		}
		return itemstack;
	}
	
	public static ItemStack gameitems(ItemStack itemstack) {
		Material type = itemstack.getType();
		if(type == Material.TNT) {
			itemstack = setName(itemstack, "�r�cTimeBomb");
			itemstack = setLore(itemstack, "�r�9Hit others to pass it on.", "�oYou don't want to have this.");
		} else if(type == Material.GOLD_BLOCK) {
			itemstack = setName(itemstack, "�r�6GoldProtection");
			itemstack = setLore(itemstack, "�r�9Run away, cause others can steal it.", "�oYou will get extra points for this.");
		}
		return itemstack;
	}
	
	public static ItemStack armor(ItemStack itemstack) {
		Material type = itemstack.getType();
		itemstack = setName(itemstack, "�r�bScore-Boots");
		if(type == Material.LEATHER_BOOTS) {
			itemstack = setLore(itemstack, "�r�9Level 1/5");
		} else if(type == Material.IRON_BOOTS) {
			itemstack = setLore(itemstack, "�r�9Level 2/5");
		} else if(type == Material.CHAINMAIL_BOOTS) {
			itemstack = setLore(itemstack, "�r�9Level 3/5");
		} else if(type == Material.GOLD_BOOTS) {
			itemstack = setLore(itemstack, "�r�9Level 4/5");
		} else if(type == Material.DIAMOND_BOOTS) {
			itemstack = setLore(itemstack, "�r�9Level 5/5");
		}
		return itemstack;
	}
	
	private static ItemStack setLore(ItemStack item, String... s) {
		ItemMeta im = item.getItemMeta();
		im.setLore(Arrays.asList(s));
		item.setItemMeta(im);
		return item;
	}
	
	private static ItemStack setName(ItemStack item, String s) {
		ItemMeta im = item.getItemMeta();
		im.setDisplayName(s);
		item.setItemMeta(im);
		return item;
	}
}
