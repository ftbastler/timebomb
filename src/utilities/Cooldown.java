package utilities;

import java.util.ArrayList;
import main.TimeBomb;

import org.bukkit.Sound;
import org.bukkit.entity.Player;


public class Cooldown {

	public static ArrayList<Player> coolplayers = new ArrayList<Player>();
	
	public Cooldown() {
		TimeBomb.instance.getServer().getScheduler().scheduleSyncRepeatingTask(TimeBomb.instance, new Runnable() {
			   public void run() {
				   if(coolplayers.size() == 0)
					   return;
				   
				   ArrayList<Player> remove = new ArrayList<Player>();
					for(Player p : coolplayers) {
						float xp = p.getExp();
						xp += 0.3;
						
						if(xp >= 1) {
							remove.add(p);
							p.setExp(1);
						} else {
							p.setExp(xp);
						}
					}
					
					for(Player p : remove) {
						coolplayers.remove(p);
						p.playSound(p.getLocation(), Sound.CLICK, 1, 0);
					}
					remove.clear();
					
			   }
			}, 0, 10);
	}
	
	public static boolean isCool(Player p) {
		return coolplayers.contains(p);
	}
	
	public static void addCool(Player p) {
		coolplayers.add(p);
		p.setExp(0);
	}
	
	public static void removeCool(Player p) {
		if(coolplayers.contains(p)) {
			coolplayers.remove(p);
			p.setExp(1);
		}
	}
	
	public static void clear() {
		coolplayers.clear();
	}
	
}
