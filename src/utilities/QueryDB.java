package utilities;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


public class QueryDB extends Thread{
	
	private String sql;
	private Connection con;
	
	public QueryDB(String sql, Connection con) {
		
		setDaemon(false);
		
		this.sql = sql;
		this.con = con;
	}
	
	@Override
	public void run() {
		MySQL.lock.lock();
		try {
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			stmt.close();
		} catch (SQLException ex) {
			System.err.println("[TimeBomb] Error with following query: " + sql);
			System.err.println("[TimeBomb] MySQL-Error: " + ex.getMessage());
		} catch (NullPointerException ex) {
			System.err.println("[TimeBomb] Error with following query (NullPointerException): " + sql);
		}
		MySQL.lock.unlock();
	}
}
