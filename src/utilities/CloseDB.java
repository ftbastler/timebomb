package utilities;

import java.sql.Connection;
import java.sql.SQLException;

public class CloseDB extends Thread{

	private Connection con;
	
	public CloseDB(Connection con) {
		setDaemon(false);
		this.con = con;
	}
	
	@Override
	public void run() {
		MySQL.lock.lock();
		try {
			System.out.println("[TimeBomb] Disconnecting from MySQL database...");
			con.close();
		} catch (SQLException ex) {
			System.err.println("[TimeBomb] Error while closing the connection...");
		} catch (NullPointerException ex) {
			System.err.println("[TimeBomb] Error while closing the connection...");
		}
		MySQL.lock.unlock();
	}
}
