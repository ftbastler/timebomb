package utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

import main.TimeBomb;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import utilities.enums.VoteType;

public class MySQL {

	public static String SQL_HOST = null;
	public static String SQL_USER = null;
	public static String SQL_PASS = null;
	public static String SQL_DATA = null;
	public static String SQL_PORT = null;
	private static Connection con = null;
	public static Boolean firstconnect = false;
	
	private static int tries = 0;
	
	public static ReentrantLock lock = new ReentrantLock(true);
	
	public MySQL() {
		connect();
		if(!firstconnect)
			return;
		query("CREATE TABLE IF NOT EXISTS `tb_PLAYERS` (`ID` int(10) unsigned NOT NULL AUTO_INCREMENT, `NAME` varchar(16) NOT NULL, `PLAYED` int(10), `DIED` int(10), `POINTS` int(10), `CASH` int(10), PRIMARY KEY (`ID`)) ENGINE=MyISAM DEFAULT CHARSET=UTF8 AUTO_INCREMENT=1 ;");
		query("CREATE TABLE IF NOT EXISTS `tb_MAPVOTES` (`ID` int(10) unsigned NOT NULL AUTO_INCREMENT, `NAME` varchar(16) NOT NULL, `UPVOTES` int(10), `DOWNVOTES` int(10), PRIMARY KEY (`ID`)) ENGINE=MyISAM DEFAULT CHARSET=UTF8 AUTO_INCREMENT=1 ;");
	}
	
	public static void connect() {
		tries++;
		
		try {
			System.out.println("[TimeBomb] Connecting to MySQL database... (" + tries + ")");
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String conn = "jdbc:mysql://" + SQL_HOST + ":" + SQL_PORT + "/" + SQL_DATA;
			con = DriverManager.getConnection(conn, SQL_USER, SQL_PASS);
			firstconnect = true;
		} catch (ClassNotFoundException ex) {
			System.err.println("[TimeBomb] No MySQL driver found!");
			TimeBomb.getInstance().getLogger().warning("Failed to connect to: \"" + "jdbc:mysql://" + SQL_HOST + ":" + SQL_PORT + "/" + SQL_DATA + "\" - U: \"" + SQL_USER + "\"" + " P: \"" + toStars(SQL_PASS) + "\"");
			firstconnect = false;
		} catch (SQLException ex) {
			System.err.println("[TimeBomb] Error while fetching MySQL connection!");
			TimeBomb.getInstance().getLogger().warning("Failed to connect to: \"" + "jdbc:mysql://" + SQL_HOST + ":" + SQL_PORT + "/" + SQL_DATA + "\" - U: \"" + SQL_USER + "\"" + " P: \"" + toStars(SQL_PASS) + "\"");
			firstconnect = false;
		} catch (Exception ex) {
			System.err.println("[TimeBomb] Unknown error while fetchting MySQL connection.");
			TimeBomb.getInstance().getLogger().warning("Failed to connect to: \"" + "jdbc:mysql://" + SQL_HOST + ":" + SQL_PORT + "/" + SQL_DATA + "\" - U: \"" + SQL_USER + "\"" + " P: \"" + toStars(SQL_PASS) + "\"");
			firstconnect = false;
		}
		
		if(tries < 3) {
			connect();
			return;
		}
		
		if(!firstconnect) {
			TimeBomb.getInstance().getLogger().warning("---------- FATAL ERROR ----------");
			TimeBomb.getInstance().getLogger().warning("COULD NOT ESTABLISH MYSQL CONNECTION!");
			TimeBomb.getInstance().getLogger().warning("Timebomb will not work without a MySQL database.");
			TimeBomb.getInstance().getLogger().warning("As a result TimeBomb is now shutting down.");
			TimeBomb.getInstance().getLogger().warning("---------------------------------");
			Bukkit.getServer().getPluginManager().disablePlugin(TimeBomb.getInstance());
		}
	}
	
	private static String toStars(String pw) {
		String stars = "";
		for(int i = 0; i < pw.length(); i++) {
			stars += "*";
		}
		return stars;
	}

	public static void query(String sql) {
		QueryDB bq = new QueryDB(sql, con);
		ExecutorService executor = Executors.newCachedThreadPool();
		executor.execute(bq);
		executor.shutdown();
	}

	public static void disconnect() {
		CloseDB end = new CloseDB(con);
		ExecutorService executor = Executors.newCachedThreadPool();
		executor.execute(end);
		executor.shutdown();
	}
	
	public static Integer getDeaths(String playername) {
		try {
			PreparedStatement stmt = con.prepareStatement("SELECT `DIED` FROM `tb_PLAYERS` WHERE `NAME` = ?;");
			stmt.setString(1, playername);
			ResultSet r = stmt.executeQuery();
			r.last();
			if (r.getRow() == 0) {
				stmt.close();
				r.close();
				return null;
			}
			Integer played = r.getInt("DIED");
			stmt.close();
			r.close();
			return played;
		} catch (SQLException ex) {
			System.err.println("[TimeBomb] MySQL-Error: " + ex.getMessage());
			return null;
		} catch (NullPointerException ex) {
			System.err.println("[TimeBomb] Error while performing a query. (NullPointerException)");
			return null;
		}
	}
	
	public static Integer getPlayedGames(String playername) {
		try {			
			PreparedStatement stmt = con.prepareStatement("SELECT `PLAYED` FROM `tb_PLAYERS` WHERE `NAME` = ?;");
			stmt.setString(1, playername);
			ResultSet r = stmt.executeQuery();
			r.last();
			if (r.getRow() == 0) {
				stmt.close();
				r.close();
				return null;
			}
			Integer played = r.getInt("PLAYED");
			stmt.close();
			r.close();
			return played;
		} catch (SQLException ex) {
			System.err.println("[TimeBomb] MySQL-Error: " + ex.getMessage());
			return null;
		} catch (NullPointerException ex) {
			System.err.println("[TimeBomb] Error while performing a query. (NullPointerException)");
			return null;
		}
	}
	
	public static void printTop5(Player p) {
		try {
			Statement stmt = con.createStatement();
			ResultSet r = stmt.executeQuery("SELECT `NAME`,`POINTS`,`CASH` FROM `tb_PLAYERS` ORDER BY `POINTS` DESC LIMIT 9;");
			r.first();
			Integer i = 1;
			p.sendMessage("�6--------- [ TimeBomb TOP5 ] ---------");
			p.sendMessage("�6[RANK]   [POINTS]   [MONEY]   [NAME]");
			while(r.getRow() != 0) {
				if(i > 5) {
					p.sendMessage("�7 " + i + "         " + r.getInt("POINTS") + "         " + r.getInt("CASH") + "         " + r.getString("NAME"));
				} else {
					p.sendMessage("�e " + i + "         " + r.getInt("POINTS") + "         " + r.getInt("CASH") + "         " + r.getString("NAME"));
				}
				i++;
				r.next();
			}
			stmt.close();
			r.close();
		} catch (SQLException ex) {
			System.err.println("[TimeBomb] MySQL-Error: " + ex.getMessage());
			return;
		} catch (NullPointerException ex) {
			System.err.println("[TimeBomb] Error while performing a query. (NullPointerException)");
			return;
		}
	}
	
	public static Boolean playerPlayedBefore(String playername) {
		try {			
			PreparedStatement stmt = con.prepareStatement("SELECT `ID`, `NAME` FROM `tb_PLAYERS` WHERE `NAME` = ?;");
			stmt.setString(1, playername);
			ResultSet r = stmt.executeQuery();
			r.last();
			if (r.getRow() == 0) {
				stmt.close();
				r.close();
				return false;
			}
			stmt.close();
			r.close();
			return true;
		} catch (SQLException ex) {
			System.err.println("[TimeBomb] MySQL-Error: " + ex.getMessage());
			return false;
		} catch (NullPointerException ex) {
			System.err.println("[TimeBomb] Error while performing a query. (NullPointerException)");
			return false;
		}
	}
	
	public static Integer getPoints(String playername) {
		try {
			PreparedStatement stmt = con.prepareStatement("SELECT `POINTS` FROM `tb_PLAYERS` WHERE `NAME` = ?;");
			stmt.setString(1, playername);
			ResultSet r = stmt.executeQuery();
			r.last();
			if (r.getRow() == 0) {
				stmt.close();
				r.close();
				return null;
			}
			Integer points = r.getInt("POINTS");
			stmt.close();
			r.close();
			return points;
		} catch (SQLException ex) {
			System.err.println("[TimeBomb] MySQL-Error: " + ex.getMessage());
			return null;
		} catch (NullPointerException ex) {
			System.err.println("[TimeBomb] Error while performing a query. (NullPointerException)");
			return null;
		}
	}
	
	public static Integer getCash(String playername) {
		try {
			PreparedStatement stmt = con.prepareStatement("SELECT `CASH` FROM `tb_PLAYERS` WHERE `NAME` = ?;");
			stmt.setString(1, playername);
			ResultSet r = stmt.executeQuery();
			r.last();
			if (r.getRow() == 0) {
				stmt.close();
				r.close();
				return null;
			}
			Integer points = r.getInt("CASH");
			stmt.close();
			r.close();
			return points;
		} catch (SQLException ex) {
			System.err.println("[TimeBomb] MySQL-Error: " + ex.getMessage());
			return null;
		} catch (NullPointerException ex) {
			System.err.println("[TimeBomb] Error while performing a query. (NullPointerException)");
			return null;
		}
	}
	
	public static Boolean mapExists(String mapname) {
		try {
			PreparedStatement stmt = con.prepareStatement("SELECT `ID`, `NAME` FROM `tb_MAPVOTES` WHERE `NAME` = ?;");
			stmt.setString(1, mapname);
			ResultSet r = stmt.executeQuery();
			r.last();
			if (r.getRow() == 0) {
				stmt.close();
				r.close();
				return false;
			}
			stmt.close();
			r.close();
			return true;
		} catch (SQLException ex) {
			System.err.println("[TimeBomb] MySQL-Error: " + ex.getMessage());
			return false;
		} catch (NullPointerException ex) {
			System.err.println("[TimeBomb] Error while performing a query. (NullPointerException)");
			return false;
		}
	}
	
	public static Integer getMapVotes(String mapname, VoteType type) {
		String getter = type == VoteType.UPVOTE ? "UPVOTES" : "DOWNVOTES";
		try {
			PreparedStatement stmt = con.prepareStatement("SELECT `" + getter + "` FROM `tb_MAPVOTES` WHERE `NAME` = ?;");
			stmt.setString(1, mapname);
			ResultSet r = stmt.executeQuery();
			r.last();
			if (r.getRow() == 0) {
				stmt.close();
				r.close();
				return null;
			}
			Integer votes = r.getInt(getter);
			stmt.close();
			r.close();
			return votes;
		} catch (SQLException ex) {
			System.err.println("[TimeBomb] MySQL-Error: " + ex.getMessage());
			return null;
		} catch (NullPointerException ex) {
			System.err.println("[TimeBomb] Error while performing a query. (NullPointerException)");
			return null;
		}
	}
	
}
