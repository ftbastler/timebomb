package utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Logger;

import main.TimeBomb;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.yaml.snakeyaml.error.YAMLException;

import timers.SignTimer;


public class ConfigurationFile {
	private static Boolean created = false;
	static File configFile = new File(TimeBomb.getInstance().getDataFolder(), "config.yml");
	static File zoneFile = new File(TimeBomb.getInstance().getDataFolder(), "zone.yml");
	private static Logger log = TimeBomb.getInstance().getLogger();
  
  public static void createFiles() {
    if (!configFile.exists()) {
      configFile.getParentFile().mkdirs();
      copy(TimeBomb.getInstance().getResource("config.yml"), configFile);
      created = true;
    }

    if (!zoneFile.exists()) {
      zoneFile.getParentFile().mkdirs();
      copy(TimeBomb.getInstance().getResource("zone.yml"), zoneFile);
    }
  }

  public static Boolean createdFiles() {
	  return created;
  }
  
  public static void loadin() {
	try {
	if(created) {
		log.info("[TimeBomb] ---------- INSTALLATION ----------");
		log.info("[TimeBomb] Created config.yml! Fill out the configuration file and restart your server.");
		log.info("[TimeBomb] ----------------------------------");
		Bukkit.getServer().getPluginManager().disablePlugin(TimeBomb.getInstance());
		return;
	}
	
	TimeBomb.lobby = new Location(Bukkit.getServer().getWorld(TimeBomb.instance.getConfig().getString("spawn.W")), TimeBomb.instance.getConfig().getInt("spawn.X"), TimeBomb.instance.getConfig().getInt("spawn.Y"), TimeBomb.instance.getConfig().getInt("spawn.Z"));
	events.GameEvent.give_msg = Boolean.valueOf(TimeBomb.instance.getConfig().getBoolean("give-msg"));
    MySQL.SQL_HOST = TimeBomb.instance.getConfig().getString("mysql.hostname");
    MySQL.SQL_USER = TimeBomb.instance.getConfig().getString("mysql.username");
    MySQL.SQL_PASS = TimeBomb.instance.getConfig().getString("mysql.password");
    MySQL.SQL_DATA = TimeBomb.instance.getConfig().getString("mysql.database");
    MySQL.SQL_PORT = TimeBomb.instance.getConfig().getString("mysql.port");
    TimeBomb.autoupdate = TimeBomb.instance.getConfig().getBoolean("auto-update");
    TimeBomb.autolobby = TimeBomb.instance.getConfig().getBoolean("auto-lobby");
    TimeBomb.lobbypvp = TimeBomb.instance.getConfig().getBoolean("lobby-pvp");
    
    ConfigurationSection cs = TimeBomb.instance.getConfig().getConfigurationSection("signs");
    for(String key : cs.getKeys(false)) {
    	if(Bukkit.getServer().getWorld(cs.getString(key + ".W")) == null)
    		continue;
    	Location l = new Location(Bukkit.getServer().getWorld(cs.getString(key + ".W")), 
    			cs.getInt(key + ".X"),
    			cs.getInt(key + ".Y"),
    			cs.getInt(key + ".Z"));
    	
    	Block b = l.getBlock();
    	if(b.getType() == Material.WALL_SIGN || b.getType() == Material.SIGN || b.getType() == Material.SIGN_POST) {
    		Sign s = (Sign) b.getState();
    		SignTimer.addSign(s);
    	} else {
    		TimeBomb.getInstance().getLogger().warning("Unable to locate sign at: " + l.toString());
    		continue;
    	}
    }
    
    if(TimeBomb.lobby == null)
    	TimeBomb.lobby = Bukkit.getServer().getWorlds().get(0).getSpawnLocation();
    
	} catch(NullPointerException e) {
		TimeBomb.getInstance().getLogger().warning("Error while loading configuration files!");
		e.printStackTrace();
	} catch(YAMLException e) {
		TimeBomb.getInstance().getLogger().warning("Error while loading configuration files!");
		e.printStackTrace();
	}
  }

  private static void copy(InputStream in, File file) {
    try {
      OutputStream out = new FileOutputStream(file);
      byte[] buf = new byte[1024];
      int len;
      while ((len = in.read(buf)) > 0)
      {
        out.write(buf, 0, len);
      }
      out.close();
      in.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}