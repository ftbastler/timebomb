package utilities.enums;

public enum ZoneCreationState {
	NONE, PLSPAWN, TBSPAWN, FINISH;
}
