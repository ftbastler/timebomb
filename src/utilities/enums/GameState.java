package utilities.enums;

public enum GameState {
	NONE, PROGRESS, DISABLED;
}
