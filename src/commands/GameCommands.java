package commands;

import java.io.File;

import main.Game;
import main.TimeBomb;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import timers.GameTimer;
import utilities.InfoBook;
import utilities.MySQL;
import utilities.ParticleEffect;
import utilities.Permissions;
import utilities.Updater;
import utilities.Voter;
import utilities.enums.GameState;
import utilities.enums.VoteType;

public class GameCommands implements CommandExecutor {

	String[] help =
		{
			"�6--------- [ TimeBomb CMD-HELP ] ---------",
			"/tb join �eJoin TimeBomb.",
			"/tb leave �eLeave TimeBomb.",
			"/tb help �eLearn how to play the game.",
			"/tb vote �eVote on the current/last map.",
			"/tbshop �eDisplay the shop to buy powerups.",
			"/tbscore �eDisplay your score.",
			"/tbscore rank �eDisplay the TOP5 players."
		};
	
	String[] adminhelp = 
		{
			"�6--------- [ TimeBomb ADMIN CMD-HELP ] ---------",
			"�c/tb <subcommand>",
			"start,enable,disable,addpoints,addcash,warp,update,reload,softreload,kickall,force"
		};
	
	String[] vote =
		{
			"�6--------- [ TimeBomb VOTE ] ---------",
			"/tb vote + �eUpvote the current/last map.",
			"/tb vote - �eDownvote the current/last map.",
		};

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "You can't use this command from a console!");
			return true;
		}
		Player p = (Player) sender;
						
		if(cmd.getName().equalsIgnoreCase("tb")) {
			if(args.length == 0) {
				if(Permissions.isAdmin(p))
					p.sendMessage(adminhelp);
				else
					p.sendMessage(help);
				return true;
			}	
						
// USER COMMANDS FROM NOW ON
			if(args[0].equalsIgnoreCase("leave")) {
				try {
					ParticleEffect.HAPPY_VILLAGER.play(p.getLocation().add(0, 2, 0), 0, 0, 0, 1, 5);
				} catch (Exception e) {
					e.printStackTrace();
				}
				Game.leave(p, false);
				return true;
			}
			
			if(args[0].equalsIgnoreCase("join")) {
				try {
					ParticleEffect.HEART.play(p.getLocation().add(0, 2, 0), 0, 0, 0, 1, 5);
				} catch (Exception e) {
					e.printStackTrace();
				}
				Game.join(p);				
				return true;
			}
			
			if(args[0].equalsIgnoreCase("help")) {
				p.getInventory().clear();
				p.getInventory().addItem(InfoBook.getBook());
				p.updateInventory();
				p.sendMessage("�aThis book should help you. :)");
				return true;
			}
			
			if(args[0].equalsIgnoreCase("vote")) {
				if(args.length != 2) {
					p.sendMessage(vote);
					return true;
				}
				if(Game.lastmap == null || Game.lastmap == "") {
					p.sendMessage("�cPlease wait for the next game to start.");
					return true;
				}
				if(!Game.players.contains(p)) {
					p.sendMessage("�cYou didn't even play on the last map.");
					return true;
				}
				
				if(args[1].equalsIgnoreCase("+")) {
					p.sendMessage("�aYou LIKED \"�r" + Game.lastmap + "�a\". �eThanks for the feedback.");
					Voter.vote(Game.lastmap.toLowerCase(), VoteType.UPVOTE);
					return true;
				}
				if(args[1].equalsIgnoreCase("-")) {
					p.sendMessage("�cYou DISLIKED \"�r" + Game.lastmap + "�c\". �eThanks for the feedback.");
					Voter.vote(Game.lastmap.toLowerCase(), VoteType.DOWNVOTE);
					return true;
				}
				p.sendMessage(vote);
				return true;
			}
			
// ADMIN COMMANDS FROM NOW ON
			if(!Permissions.isAdmin(p)) {
				p.sendMessage("�cYou are not allowed to use this command!");
				return true;
			}
							
			if(args[0].equalsIgnoreCase("start")) {
				GameTimer.cancel();
				Game.startGame();
				return true;
			}
			
			if(args[0].equalsIgnoreCase("update")) {
				Updater updater = new Updater();
				if(updater.checkForUpdates())
					p.sendMessage(ChatColor.GREEN + "New version available: �r" + updater.getNewVersionString());
				else
					p.sendMessage(ChatColor.GREEN + "No updates have been found.");
			}
			
			if(args[0].equalsIgnoreCase("disable")) {
				if(Game.state != GameState.DISABLED) {
					Game.state = GameState.DISABLED;
					Game.messagePlayers("�cTimeBomb got disabled! Kicking all players...");
					GameTimer.cancel();
					Game.kickall();
					p.sendMessage("�cDisabled TimeBomb! �eDo �r/tb enable �eto enable it again.");
				} else {
					p.sendMessage("�cTimeBomb is already disabled.");
				}
				return true;
			}
			
			if(args[0].equalsIgnoreCase("enable")) {
				if(Game.state == GameState.DISABLED) {
					Game.state = GameState.NONE;
					new GameTimer();
					p.sendMessage("�aEnabled TimeBomb!");
				} else {
					p.sendMessage("�cTimeBomb is already enabled.");
				}
				return true;
			}
			
			if(args[0].equalsIgnoreCase("reload")) {
				TimeBomb.reloadTimeBomb();
				p.sendMessage("�aTimeBomb has been reloaded.");
				return true;
			}
			
			if(args[0].equalsIgnoreCase("softreload")) {
				if(Game.state == GameState.PROGRESS) {
					Game.reload = true;
					p.sendMessage("�aTimeBomb will be reloaded after the current game ends.");
				} else {
					TimeBomb.reloadTimeBomb();
					p.sendMessage("�aTimeBomb has been reloaded.");
				}
				return true;
			}
			
			if(args[0].equalsIgnoreCase("kickall")) {
				Game.messagePlayers("�cKicking all players from TimeBomb...");
				Game.kickall();
				p.sendMessage("�aKicked all players from TimeBomb!");
				return true;
			}
			
			if(args[0].equalsIgnoreCase("force")) {
				if(args.length != 2) {
					p.sendMessage("�cUsage: �r/tb force <zone>");
					return true;
				}
				File zoneFile = new File(TimeBomb.getInstance().getDataFolder(), "zone.yml");
				FileConfiguration f = YamlConfiguration.loadConfiguration(zoneFile);
				if(!f.isSet(args[1].toLowerCase() + ".plspawn") || !f.isSet(args[1].toLowerCase() + ".tbspawn")) {
					p.sendMessage("�cZone doesn't exist!");
					return true;
				} else {
					ConfigurationSection fs = f.getConfigurationSection(args[1]);
					Game.nextmap = fs.getName();
					p.sendMessage("�aForcing next map to be �r\"" + fs.getString("name") + "\"�a!");
					return true;
				}
			}
			
			if(args[0].equalsIgnoreCase("warp")) {
				if(args.length != 2) {
					p.sendMessage("�cUsage: �r/tb warp <zone>");
					return true;
				}
				if(args[1].equalsIgnoreCase("spawn") || args[1].equalsIgnoreCase("lobby")) {
					p.teleport(TimeBomb.lobby);
					p.sendMessage("�aTeleported to �rlobby�a.");
					return true;
				}
				
				File zoneFile = new File(TimeBomb.getInstance().getDataFolder(), "zone.yml");
				FileConfiguration f = YamlConfiguration.loadConfiguration(zoneFile);
				if(!f.isSet(args[1].toLowerCase() + ".plspawn") || !f.isSet(args[1].toLowerCase() + ".tbspawn")) {
					p.sendMessage("�cZone doesn't exist!");
					return true;
				} else {
					ConfigurationSection fs = f.getConfigurationSection(args[1]);
					Double x = Double.parseDouble(fs.getInt("plspawn.X") + "");
					Double y = Double.parseDouble(fs.getInt("plspawn.Y") + "");
					Double z = Double.parseDouble(fs.getInt("plspawn.Z") + "");
					World w = Bukkit.getServer().getWorld(fs.getString("plspawn.W"));
					p.teleport(new Location(w, x, y, z));
					p.sendMessage("�aTeleported to �r" + args[1] + "�a.");
					return true;
				}
			}
						
			if(args[0].equalsIgnoreCase("addpoints")) {
				if(args.length != 3) {
					p.sendMessage("�cUsage: �r/tb addpoints <player> <amount>");
					return true;
				}
				if(!MySQL.playerPlayedBefore(args[1])) {
					p.sendMessage("�cUnknow player.");
					return true;
				}
				args[1] = args[1].replace("'", "");
				args[2] = args[2].replace("'", "");
				args[1] = args[1].replace("`", "");
				args[2] = args[2].replace("`", "");
				MySQL.query("UPDATE `tb_PLAYERS` SET `POINTS` = `POINTS` + " + args[2] + " WHERE `NAME` = '" + args[1] + "';");
				p.sendMessage("�aAdded " + args[2] + " points to player " + args[1]);
				return true;
			}
			
			if(args[0].equalsIgnoreCase("addcash")) {
				if(args.length != 3) {
					p.sendMessage("�cUsage: �r/tb addcash <player> <amount>");
					return true;
				}
				if(!MySQL.playerPlayedBefore(args[1])) {
					p.sendMessage("�cUnknow player.");
					return true;
				}
				args[1] = args[1].replace("'", "");
				args[2] = args[2].replace("'", "");
				args[1] = args[1].replace("`", "");
				args[2] = args[2].replace("`", "");
				MySQL.query("UPDATE `tb_PLAYERS` SET `CASH` = `CASH` + " + args[2] + " WHERE `NAME` = '" + args[1] + "';");
				p.sendMessage("�aAdded " + args[2] + " $ to player " + args[1]);
				return true;
			}
									
			if(Permissions.isAdmin(p))
				p.sendMessage(adminhelp);
			else
				p.sendMessage(help);
			return true;
			
		}
		
		return true;
	}
	
}
