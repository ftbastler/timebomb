package commands;

import java.util.ArrayList;
import java.util.HashMap;

import main.Game;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import utilities.MySQL;
import utilities.ItemNamer;

public class ShopCommands implements CommandExecutor {

	public static HashMap<Player, ArrayList<ItemStack>> cart = new HashMap<Player, ArrayList<ItemStack>>();

	
	String[] shop =
		{
			"�6--------- [ TimeBomb SHOP ] ---------",
			"�e�lAvailable items:",
			"�eSnowball �o[3$]�r�e - Nearby players will get a bad potions effect.",
			"�eSlimeball �o[4$]�r�e - Nearby players will be launched.",
			"�eFeather �o[2$]�r�e - Gives you a speed boost.",
			"�eStick �o[2$]�r�e - Freezes a player.",
			"",
			"�cTo buy an item, do �r/tbshop <itemname> <amount>�c.",
			"�cTo view your cart, do �r/tbshop cart�c."
		};
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)){
			sender.sendMessage(ChatColor.RED + "You can't use this command!");
			return true;
		}
		
		Player p = (Player) sender;
		
		if(cmd.getName().equalsIgnoreCase("tbscore")) {
			if(args.length == 1) {
				if(args[0].equalsIgnoreCase("rank")) {
					MySQL.printTop5(p);
					return true;
				}
				
				if(MySQL.playerPlayedBefore(args[0])) {
					p.sendMessage("�e" + args[0] + " has currently " + MySQL.getPoints(args[0]) + " points and " + MySQL.getCash(args[0]) + "$.");
					return true;
				} else {
					p.sendMessage("�cUnknown player.");
					return true;
				}
			}
			
			p.sendMessage("�eYou have currently " + MySQL.getPoints(p.getName()) + " points and " + MySQL.getCash(p.getName()) + "$.");
			return true;

		}
		
		if(cmd.getName().equalsIgnoreCase("tbshop")) {
			
			if(args.length == 0) {
				p.sendMessage(shop);
				p.sendMessage("�eYou have currently " + MySQL.getCash(p.getName()) + "$.");
				return true;
			}
			
			if(args.length != 2 & !args[0].equalsIgnoreCase("cart")) {
				p.sendMessage(shop);
				return true;
			}
			
			if(args[0].equalsIgnoreCase("cart")) {
				ArrayList<ItemStack> usercart = cart.get(p);
				if(usercart == null) {
					p.sendMessage("�eYour cart is currently empty.");
					return true;
				}
				
				p.sendMessage("�6--------- [ TimeBomb CART ] ---------");
				for(ItemStack i : usercart) {
					Integer amount = i.getAmount();
					Material item = i.getType();
					String itemname = null;
					if(item == Material.SNOW_BALL) {
						itemname = "Snowball Grenade";
					} else if(item == Material.SLIME_BALL) {
						itemname = "Slimeball Grenade";
					} else if(item == Material.FEATHER) {
						itemname = "Magic Feather";
					} else if(item == Material.STICK) {
						itemname = "Magic Stick";
					} else {
						itemname = item.toString();
					}
					
					p.sendMessage("�e" + amount + "x " + itemname);
				}

				return true;
			}
			
			if(args[0].equalsIgnoreCase("snowball")) {
				Integer amount = 0;
				
				try {
					amount = Integer.parseInt(args[1]);
				} catch(NumberFormatException e) { 
					p.sendMessage("�cAmount has to be a number.");
					return true; 
			    }
				
				if(amount == 0) {
					p.sendMessage("�cAmount has to be more than 0.");
					return true;
				}
				
				Integer cash = MySQL.getCash(p.getName());
				Integer cost = amount * 3;
				if(cash < cost) {
					p.sendMessage("�cYou don't have enough money!");
					return true;
				}
				MySQL.query("UPDATE `tb_PLAYERS` SET `CASH` = `CASH` - " + cost + " WHERE `NAME` = '" + p.getName() + "';");

				if(Game.gamers.contains(p)) {
					p.getInventory().addItem(ItemNamer.powerup(new ItemStack(Material.SNOW_BALL, amount)));
					p.updateInventory();
				} else {
					ArrayList<ItemStack> a = new ArrayList<ItemStack>();
					if(cart.containsKey(p)) {
						a = cart.get(p);
						cart.remove(p);
					}
					a.add(ItemNamer.powerup(new ItemStack(Material.SNOW_BALL, amount)));
					cart.put(p, a);
				}
				p.sendMessage("�aThanks for shopping ;)");
				return true;
			}
								
				
				if(args[0].equalsIgnoreCase("slimeball")) {
					Integer amount = 0;
					
					try {
						amount = Integer.parseInt(args[1]);
					} catch(NumberFormatException e) { 
						p.sendMessage("�cAmount has to be a number.");
						return true; 
				    }
					
					Integer cash = MySQL.getCash(p.getName());
					Integer cost = amount * 3;
					if(cash < cost) {
						p.sendMessage("�cYou don't have enough money!");
						return true;
					}
					MySQL.query("UPDATE `tb_PLAYERS` SET `CASH` = `CASH` - " + cost + " WHERE `NAME` = '" + p.getName() + "';");

					if(Game.gamers.contains(p)) {
						p.getInventory().addItem(ItemNamer.powerup(new ItemStack(Material.SLIME_BALL, amount)));
						p.updateInventory();
					} else {
						ArrayList<ItemStack> a = new ArrayList<ItemStack>();
						if(cart.containsKey(p)) {
							a = cart.get(p);
							cart.remove(p);
						}
						a.add(ItemNamer.powerup(new ItemStack(Material.SLIME_BALL, amount)));
						cart.put(p, a);
					}
					p.sendMessage("�aThanks for shopping ;)");
					return true;
				}
				
				if(args[0].equalsIgnoreCase("feather")) {
					Integer amount = 0;
					
					try {
						amount = Integer.parseInt(args[1]);
					} catch(NumberFormatException e) { 
						p.sendMessage("�cAmount has to be a number.");
						return true; 
				    }
					
					Integer cash = MySQL.getCash(p.getName());
					Integer cost = amount * 3;
					if(cash < cost) {
						p.sendMessage("�cYou don't have enough money!");
						return true;
					}
					MySQL.query("UPDATE `tb_PLAYERS` SET `CASH` = `CASH` - " + cost + " WHERE `NAME` = '" + p.getName() + "';");
					
					if(Game.gamers.contains(p)) {
						p.getInventory().addItem(ItemNamer.powerup(new ItemStack(Material.FEATHER, amount)));
						p.updateInventory();
					} else {
						ArrayList<ItemStack> a = new ArrayList<ItemStack>();
						if(cart.containsKey(p)) {
							a = cart.get(p);
							cart.remove(p);
						}
						a.add(ItemNamer.powerup(new ItemStack(Material.FEATHER, amount)));
						cart.put(p, a);
					}
					p.sendMessage("�aThanks for shopping ;)");
					return true;
				}
				
				if(args[0].equalsIgnoreCase("stick")) {
					Integer amount = 0;
					
					try {
						amount = Integer.parseInt(args[1]);
					} catch(NumberFormatException e) { 
						p.sendMessage("�cAmount has to be a number.");
						return true; 
				    }
					
					Integer cash = MySQL.getCash(p.getName());
					Integer cost = amount * 3;
					if(cash < cost) {
						p.sendMessage("�cYou don't have enough money!");
						return true;
					}
					MySQL.query("UPDATE `tb_PLAYERS` SET `CASH` = `CASH` - " + cost + " WHERE `NAME` = '" + p.getName() + "';");
					
					if(Game.gamers.contains(p)) {
						p.getInventory().addItem(ItemNamer.powerup(new ItemStack(Material.STICK, amount)));
						p.updateInventory();
					} else {
						ArrayList<ItemStack> a = new ArrayList<ItemStack>();
						if(cart.containsKey(p)) {
							a = cart.get(p);
							cart.remove(p);
						}
						a.add(ItemNamer.powerup(new ItemStack(Material.STICK, amount)));
						cart.put(p, a);
					}
					p.sendMessage("�aThanks for shopping ;)");
					return true;
				}
				
				p.sendMessage("�cUsage: �r/tbshop <itemname> <amount> �cor �r/shop");
				return true;
			}
		
		return true;
	}
	
}
