package commands;

import java.io.File;
import java.io.IOException;


import main.TimeBomb;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import utilities.MySQL;
import utilities.Permissions;
import utilities.Voter;
import utilities.enums.VoteType;
import utilities.enums.ZoneCreationState;


public class ZoneCommands implements CommandExecutor {
	
	private ZoneCreationState zstate = ZoneCreationState.NONE;
	private String zname;
	private Location plspawn;
	private Location tbspawn;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)){
			sender.sendMessage(ChatColor.RED + "You can't use this command!");
			return true;
		}
		
		Player p = (Player) sender;
				
		if(cmd.getName().equalsIgnoreCase("tbzone")) {
			if(!Permissions.isAdmin(p)) {
				p.sendMessage("�cYou are not allowed to use this command!");
				return true;
			}
			
			if(args.length < 1 || args.length > 2) {
				p.sendMessage("�cUsage: �r/tbzone <list:lobby:create:remove>");
				return true;
			}
			
				File zoneFile = new File(TimeBomb.getInstance().getDataFolder(), "zone.yml");
				FileConfiguration f = YamlConfiguration.loadConfiguration(zoneFile);
				
				if(args[0].equalsIgnoreCase("list")) {
					try {
						p.sendMessage("�6--------- [ TimeBomb ZONES ] ---------");
						for(String key : f.getKeys(false)){
							p.sendMessage("�e" + key + " (\"" + f.getConfigurationSection(key).getString("name") + "\") - VOTES:" + ChatColor.RESET + "" + ChatColor.GREEN + + MySQL.getMapVotes(f.getConfigurationSection(key).getString("name"), VoteType.UPVOTE) + " " + ChatColor.RED + + MySQL.getMapVotes(f.getConfigurationSection(key).getString("name"), VoteType.DOWNVOTE));
						}
					} catch (NullPointerException e) {
						e.printStackTrace();
						TimeBomb.getInstance().getLogger().warning("COULD NOT ESTABLISH MYSQL CONNECTION! PLUGIN WILL NOT WORK!");
					}
					return true;
				}
				
				if(args[0].equalsIgnoreCase("lobby")) {
					File configFile = new File(TimeBomb.getInstance().getDataFolder(), "config.yml");
					FileConfiguration fconfig = YamlConfiguration.loadConfiguration(configFile);
					Location loc = p.getLocation();
					fconfig.set("spawn.X", loc.getX());
					fconfig.set("spawn.Y", loc.getY());
					fconfig.set("spawn.Z", loc.getZ());
					fconfig.set("spawn.W", loc.getWorld().getName());
					TimeBomb.lobby = loc;
					try {
						fconfig.save(configFile);
					} catch (IOException e) {
						e.printStackTrace();
					}
					p.sendMessage("�aLobby has been successfully set!");
					return true;
				}
				
				if(args[0].equalsIgnoreCase("create")) {
					if(args[1].equalsIgnoreCase("plspawn") && zstate == ZoneCreationState.PLSPAWN) {
						plspawn = p.getLocation();
						zstate = ZoneCreationState.TBSPAWN;
						p.sendMessage("�aPlayer spawn set. Next: �r/tbzone create tbspawn");
						return true;
					}
					if(args[1].equalsIgnoreCase("tbspawn") && zstate == ZoneCreationState.TBSPAWN) {
						tbspawn = p.getLocation();
						zstate = ZoneCreationState.FINISH;
						saveZone();
						Voter.addMySQLMap(zname);
						p.sendMessage("�aZone created.");
						return true;
					}
						if(!f.isSet(args[1].toLowerCase()) && zstate == ZoneCreationState.NONE) {
							zname = args[1];
							zstate = ZoneCreationState.PLSPAWN;
							p.sendMessage("�aZone creation started. Next: �r/tbzone create plspawn");
							return true;
						} else {
							p.sendMessage("�cA zone with that name already exists! �r/tbzone create <name>");
							return true;
						}
				}
				
				if(args[0].equalsIgnoreCase("remove")) {
					if(args.length == 2 && f.isSet(args[1].toLowerCase())) {
						f.set(args[1].toLowerCase(), null);
						try {
							f.save(zoneFile);
						} catch (IOException e) {
							e.printStackTrace();
						}
						p.sendMessage("�aZone removed.");
						return true;
					} else {
						p.sendMessage("�cA zone with that name doesn't exist! �r/tbzone remove <name>");
						return true;
					}
				}
				
				p.sendMessage("�cUsage: �r/tbzone <list:lobby:create:remove>");
				return true;
			}
		
		return true;
	}
	
	private void saveZone() {
		if(zstate != ZoneCreationState.FINISH)
			return;
		
		File zoneFile = new File(TimeBomb.getInstance().getDataFolder(), "zone.yml");
		FileConfiguration f = YamlConfiguration.loadConfiguration(zoneFile);
		f.createSection(zname.toLowerCase());
		
		ConfigurationSection fs = f.getConfigurationSection(zname.toLowerCase());
		fs.set("name", zname);
		
		fs.set("plspawn.X", plspawn.getX());
		fs.set("plspawn.Y", plspawn.getY());
		fs.set("plspawn.Z", plspawn.getZ());
		fs.set("plspawn.W", plspawn.getWorld().getName());
		
		fs.set("tbspawn.X", tbspawn.getX());
		fs.set("tbspawn.Y", tbspawn.getY());
		fs.set("tbspawn.Z", tbspawn.getZ());
		fs.set("tbspawn.W", tbspawn.getWorld().getName());
		
		zstate = ZoneCreationState.NONE;
		tbspawn = null;
		plspawn = null;
		zname = null;
		try {
			f.save(zoneFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
