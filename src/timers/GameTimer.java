package timers;

import main.Game;
import main.TimeBomb;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.Objective;

public class GameTimer {

	private static Boolean timer_running = false;
	private static Integer shed_id = null;
	public static Integer countdown = 60;
	
	public GameTimer() {
		if(!timer_running) {
			if(shed_id != null)
				Bukkit.getServer().getScheduler().cancelTask(shed_id);
			
			timer_running = true;
			countdown = 60;
			shed_id = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(TimeBomb.getInstance(), new Runnable() {
				@Override
				public void run() {
					Objective o = Game.board.getObjective("timer");
					o.getScore(Bukkit.getOfflinePlayer(ChatColor.AQUA + "Countdown")).setScore(countdown);
					o.getScore(Bukkit.getOfflinePlayer(ChatColor.AQUA + "Players")).setScore(Game.players.size());
					
					countdown--;
					if(countdown <= 0) {
						timer_running = false;
						Game.startGame();
						cancel();
					}
				}
			}, 0, 20); 
		}
	}	
	
	public static void cancel() {
		if(shed_id != null) {
			Bukkit.getServer().getScheduler().cancelTask(shed_id);
			timer_running = false;
			countdown = 60;
			shed_id = null;
		}
	}
}
