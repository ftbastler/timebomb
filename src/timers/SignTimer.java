package timers;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.block.Sign;

import utilities.enums.GameState;

import main.Game;
import main.TimeBomb;

public class SignTimer {

	private static ArrayList<Sign> signs = new ArrayList<Sign>(); 
	private static Integer shed_id = null;
	
	private static ArrayList<String> msg = new ArrayList<String>();
	private static ArrayList<String> msgc = new ArrayList<String>(); 
	private static String cur_msg = "";
	private static String cur_color = "";
	private static String temp_msg = "";
	private static Integer c = 0;
	
	public SignTimer() {
		cancel();
		shed_id = TimeBomb.instance.getServer().getScheduler().scheduleSyncRepeatingTask(TimeBomb.instance, new Runnable() {
			@Override
			public void run() {
				if(cur_msg == "" || cur_msg == null || c >= cur_msg.length()) {
					if(msg.size() > 0 && msgc.size() > 0) {
						cur_msg = msg.get(0);
						msg.remove(0);
						cur_color = msgc.get(0);
						msgc.remove(0);
					} else {
						cur_msg = "Play TimeBomb, the most explosive tag game!";
						cur_msg += "                ";
						cur_color = "�b";
					}
					c = 0;
				}
				temp_msg += cur_msg.charAt(c);
				if(temp_msg.length() >= 16)
					temp_msg = temp_msg.substring(temp_msg.length() - 16);
				c++;
				updateSigns(cur_color, temp_msg);
			}
		}, 0, 5);
	}
	
	
	public static void cancel() {
		if(shed_id != null) {
			Bukkit.getServer().getScheduler().cancelTask(shed_id);
			shed_id = null;
			msgc.clear();
			msg.clear();
		}
	}
	
	public static void addSign(Sign s) {
		signs.add(s);
	}
	
	public static void remSign(Sign s) {
		signs.remove(s);
	}
	
	public static Boolean isSign(Sign s) {
		for(Sign sign : signs) {
			if(s.getBlock().getLocation().getX() == sign.getBlock().getLocation().getX() &&
				s.getBlock().getLocation().getY() == sign.getBlock().getLocation().getY() &&
				s.getBlock().getLocation().getZ() == sign.getBlock().getLocation().getZ() &&
				s.getBlock().getLocation().getWorld().getName() == sign.getBlock().getLocation().getWorld().getName())
				return true;
			else
				continue;
		}
		
		return false;
	}
	
	private static void updateSigns(String color, String message) {
		for(Sign sign : signs) {
			sign.setLine(0, color + message);
			
			if(Game.reload) {
				sign.setLine(1, "�nReloading");
				sign.setLine(2, "");
				sign.setLine(3, "");
				sign.update();
				return;
			}
			
			if(Game.state == GameState.PROGRESS) {
				sign.setLine(1, "�nIn Progress");
				sign.setLine(2, "" + Timer.time_left + " sec");
				sign.setLine(3, "" + Game.players.size() + " players");
			} else if(Game.state == GameState.NONE) {
				sign.setLine(1, "�nIn Lobby");
				sign.setLine(2, "" + GameTimer.countdown + " sec");
				sign.setLine(3, "" + Game.players.size() + " players");
			} else if(Game.state == GameState.DISABLED) {
				sign.setLine(1, "�nDisabled");
				sign.setLine(2, "");
				sign.setLine(3, "");
			}
			sign.update();
		}
	}
	
	public static void setMessage(String message, String color) {
		msg.add(message + "                ");
		msgc.add(color);
	}
	
//	private void sendSignChange(Player player, Sign sign) {
//		CraftPlayer cplayer = (CraftPlayer) player;
//		cplayer.getHandle().netServerHandler.sendPacket(new Packet130UpdateSign(sign.getX(), sign.getY(), sign.getZ(), sign.getLines()));
//	}
}
