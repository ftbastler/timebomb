package timers;

import java.util.Random;

import main.Game;
import main.TimeBomb;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import utilities.Permissions;
import utilities.ItemNamer;
import utilities.enums.GameState;

public class PowerupTimer {

	private static Integer shed_id = null;
	
	public PowerupTimer() {
		shed_id = TimeBomb.instance.getServer().getScheduler().scheduleSyncRepeatingTask(TimeBomb.instance, new Runnable() {
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				   if(Game.state == GameState.PROGRESS && Game.gamers.size() <= 40) {
					   for(Player p : Bukkit.getServer().getOnlinePlayers()) {
						   if(Permissions.isVIP(p) && Game.gamers.contains(p)) {
							   Random r = new Random();
							   ItemStack powerup = new ItemStack(Material.SNOW_BALL, 1);
							   
							  switch (r.nextInt(3)) {
							case 0:
								powerup = new ItemStack(Material.SNOW_BALL, 1);
								break;
							case 1:
								powerup = new ItemStack(Material.SLIME_BALL, 1);
								break;
							case 2:
								powerup = new ItemStack(Material.FEATHER, 1);
								break;
							default:
								break;
							}
							  
							   p.getInventory().addItem(ItemNamer.powerup(powerup));
							   p.sendMessage("�aYou were given a random powerup.");
						   }
						   p.updateInventory();
					   }
				   } else {
					   cancel();
				   }
			   }
			}, 140, 600);
	}
	
	public static void cancel() {
		if(shed_id != null) {
			Bukkit.getServer().getScheduler().cancelTask(shed_id);
			shed_id = null;
		}
	}
	
}
