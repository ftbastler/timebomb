package timers;

import main.Game;
import main.TimeBomb;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Objective;

public class Timer {
	
	private static Integer shed_id = null;
	public static Integer time_left = 0;
	
	public Timer() {
		cancel();
		time_left = 60;
		shed_id = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(TimeBomb.getInstance(), new Runnable() {
			@Override
			public void run() {		
				if(time_left == 0) {
					Game.endGame();
					time_left = 0;
					cancel();
				} else {
					if(Game.gamers != null) {
						Objective o = Game.board.getObjective("timer");
						o.getScore(Bukkit.getOfflinePlayer(ChatColor.AQUA + "Countdown")).setScore(time_left);
						o.getScore(Bukkit.getOfflinePlayer(ChatColor.AQUA + "Players")).setScore(Game.gamers.size());
					}
					time_left--;
					
					if(time_left == 2 && Game.gamers != null) {
						for(Player player : Game.gamers)
							player.playSound(player.getLocation(), Sound.CREEPER_HISS, 1, 1);
					}
				}
			}
		}, 0, 20);
	}
	
	
	public static void cancel() {
		if(shed_id != null) {
			Bukkit.getServer().getScheduler().cancelTask(shed_id);
			shed_id = null;
		}
	}
}
