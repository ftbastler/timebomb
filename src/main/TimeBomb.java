package main;

/*
 * I wrote this plugin like ages ago, when I didn't know much about Java.
 * Please don't look at the source, it's very badly coded.
 * 
 * Copyright by ftbastler
 */

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

import timers.GameTimer;
import timers.PowerupTimer;
import timers.SignTimer;
import utilities.ConfigurationFile;
import utilities.Cooldown;
import utilities.Metrics;
import utilities.MySQL;
import utilities.Updater;

import commands.GameCommands;
import commands.ShopCommands;
import commands.ZoneCommands;
import events.GameEvent;
import events.GamerEvent;

public class TimeBomb extends JavaPlugin {

	public static Logger log;
	
	public static TimeBomb instance = null;
	public static Location lobby = null;
	public static Boolean autoupdate = true;
	public static Boolean autolobby = false;
	public static Boolean lobbypvp = false;
	
	private final GamerEvent pE = new GamerEvent();
	private final GameEvent gE = new GameEvent();
	
	@Override
	public void onEnable() {
		TimeBomb.instance = this;
		Game.reload = false;
		
		try {
		    Metrics metrics = new Metrics(this);
		    metrics.start();
		} catch (IOException e) {
		    log.warning("[TimeBomb] Could not start Metrics!");
		}
		
		ConfigurationFile.createFiles();
		ConfigurationFile.loadin();
		if(!ConfigurationFile.createdFiles()) {
			new MySQL();
			if(MySQL.firstconnect) {
				registerEvents();
				registerCommands();
				
				Game.board = Bukkit.getScoreboardManager().getNewScoreboard();
				Game.board.registerNewObjective("timer", "dummy");
				Objective o = Game.board.getObjective("timer");
				o.setDisplayName(ChatColor.RED + "TimeBomb");
				o.setDisplaySlot(DisplaySlot.SIDEBAR);
				
				new Cooldown();
				new GameTimer();
				new PowerupTimer();
				new SignTimer();
				
				log.info(this + " enabled!");
			}
		}
		
		Updater updater = new Updater();
		if(updater.checkForUpdates())
			getLogger().info(ChatColor.GREEN + "New version available: �r" + updater.getNewVersionString());
		else
			getLogger().info(ChatColor.GREEN + "No updates have been found.");
	}
	
	@Override
	public void onDisable() {
		Bukkit.getServer().getScheduler().cancelAllTasks();
		if(Game.reload)
			Game.messagePlayers("�cTimeBomb is reloading. Kicking all players...");
		else
			Game.messagePlayers("�cTimeBomb gets disabled. Kicking all players...");
		
		Game.kickall();
				
		if(MySQL.firstconnect)
			MySQL.disconnect();
		log.info(this + " disabled!");
	}
	
	@Override
	public void onLoad() {
		TimeBomb.instance = this;
		TimeBomb.log = getLogger();
	}
	
	private void registerEvents() {
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(gE, this);
		pm.registerEvents(pE, this);
	}
	
	private void registerCommands() {
		getCommand("tbzone").setExecutor(new ZoneCommands());
		getCommand("tb").setExecutor(new GameCommands());
		getCommand("tbshop").setExecutor(new ShopCommands());
		getCommand("tbscore").setExecutor(new ShopCommands());
	}
	
	public static TimeBomb getInstance() {
		return instance;
	}
	
	public static void reloadTimeBomb() {
	    getInstance().reloadConfig();
	    Bukkit.getServer().getPluginManager().disablePlugin(getInstance());
	    Bukkit.getServer().getPluginManager().enablePlugin(getInstance());
	}
	
	public static File getPluginFile() {
		return getInstance().getFile();
	}
}
