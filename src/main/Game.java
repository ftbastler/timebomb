package main;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.util.Vector;
import org.kitteh.tag.TagAPI;

import commands.ShopCommands;

import timers.EffectTimer;
import timers.GameTimer;
import timers.PowerupTimer;
import timers.SignTimer;
import timers.Timer;
import utilities.Cooldown;
import utilities.FireworkEffects;
import utilities.Freeze;
import utilities.MySQL;
import utilities.ParticleEffect;
import utilities.Permissions;
import utilities.Points;
import utilities.ItemNamer;
import utilities.enums.GameState;
import utilities.enums.GameType;

public class Game {
	
	public static Boolean freeze = false;
	public static GameState state = GameState.NONE;
	public static GameType type = GameType.CLASSIC;
	public static Location tbspawn = TimeBomb.lobby;
	public static Location plspawn = TimeBomb.lobby;
	public static ArrayList<Player> tnt = new ArrayList<Player>();
	public static ArrayList<Player> gamers = new ArrayList<Player>();
	public static ArrayList<Player> players = new ArrayList<Player>();
	public static ArrayList<Player> gold = new ArrayList<Player>();
	public static Boolean reload = false;
	public static String lastmap = "";
	public static String nextmap = "";
	private static Logger log = TimeBomb.getInstance().getLogger();
	private static HashMap<Player, ItemStack[]> storedinv = new HashMap<Player, ItemStack[]>();
	
	public static Scoreboard board;
	
	private static Integer countdown = 3;
	private static Integer shed_id = null;
	
	@SuppressWarnings("deprecation")
	public static void startGame() {
		
		if(state == GameState.PROGRESS) {
//			log.info("[TimeBomb] Game is already in progress!");
			new GameTimer();
			return;
		}
		
		if(state == GameState.DISABLED) {
//			log.info("[TimeBomb] GameTimer is paused!");
			return;	
		}
		
		ConfigurationSection cf = getZone();
		if(cf == null) {
			log.warning("[TimeBomb] No zones set! Set zones first!");
			new GameTimer();
			return;
		}		
		
		gamers.clear();
		tnt.clear();
		gold.clear();
		Freeze.clear();
		
		for(Player p : Bukkit.getServer().getOnlinePlayers()) {
			if(players.contains(p)) {
				if(p.isDead() || p.isSleeping()) {
					continue;
				}
				if(p.isInsideVehicle())
					p.getVehicle().eject();
				
				gamers.add(p);
			}
		}
		
		if(gamers.size() < 2) {
//			log.info("[TimeBomb] Not enough players online to start game.");
			gamers.clear();
			tnt.clear();
			gold.clear();
			new GameTimer();
			return;
		}
		
		Random r = new Random();
		if(r.nextInt(10) >= 3)
			type = GameType.CLASSIC;
		else
			type = GameType.INFECTED;
		
		if(gamers.size() < 3)
			type = GameType.CLASSIC;
		
		String mapname = cf.getString("name");
		World w = Bukkit.getServer().getWorld(cf.getString("plspawn.W"));
		plspawn = new Location(w, cf.getInt("plspawn.X"), cf.getInt("plspawn.Y"), cf.getInt("plspawn.Z"));
		tbspawn = new Location(w, cf.getInt("tbspawn.X"), cf.getInt("tbspawn.Y"), cf.getInt("tbspawn.Z"));
		lastmap = mapname;
				
		w.loadChunk(w.getChunkAt(plspawn));
		w.loadChunk(w.getChunkAt(tbspawn));
		w.setTime(0);
		
		Timer.cancel();
				
		for(Player p : gamers) {
			p.getInventory().clear();
			p.getInventory().setHelmet(null);
			p.getInventory().setChestplate(null);
			p.getInventory().setLeggings(null);
			p.getInventory().setBoots(null);
			p.setFlying(false);
			p.setHealth(20);
			p.setFoodLevel(20);
			Points.setArmor(p);
			
			for (PotionEffect effect : p.getActivePotionEffects())
		        p.removePotionEffect(effect.getType());
						
			MySQL.query("UPDATE `tb_PLAYERS` SET `PLAYED` = `PLAYED` + 1 WHERE `NAME` = '" + p.getName() + "';");
			
			if(Permissions.isVIP(p))
				p.getInventory().addItem(ItemNamer.powerup(new ItemStack(Material.SNOW_BALL, 3)));
			
			if(ShopCommands.cart.containsKey(p)) {
				ArrayList<ItemStack> a = new ArrayList<ItemStack>();
				a = ShopCommands.cart.get(p);
				for(ItemStack i : a) {
					if(i != null)
						p.getInventory().addItem(i);
				}
				ShopCommands.cart.remove(p);
			}
						
			p.teleport(plspawn);
			p.setVelocity(new Vector(1 - r.nextInt(3), 0.5, 1 - r.nextInt(3)));
			
			p.setGameMode(GameMode.ADVENTURE);
			p.playSound(p.getLocation(), Sound.NOTE_PLING, 1, 1);
			
			p.setScoreboard(board);
			p.updateInventory();
		}
		
		Integer fegamer = 5;
		if(type == GameType.INFECTED)
			fegamer = 10;
		
		Integer tntplayers = 0;
		Integer players = gamers.size();
		tntplayers++;
		while(players >= fegamer) {
			tntplayers++;
			players = players - fegamer;
		}
		
		while (tntplayers > 0) {	
			Player p = gamers.get(r.nextInt(gamers.size()));
			if(!tnt.contains(p)) {
				tnt.add(p);
				tntplayers--;
			}
		}
		
		for(Player p : tnt) {
			p.getInventory().setItem(8, ItemNamer.gameitems(new ItemStack(Material.TNT, 1)));
			p.updateInventory();
			p.sendMessage("�bYou were randomly chosen to be given a TimeBomb!");
			p.playSound(p.getLocation(), Sound.FUSE, 1, 1);
			Cooldown.addCool(p);
			setNameTag(p, ChatColor.RED);
			
			p.teleport(tbspawn);
			p.setVelocity(new Vector(1 - r.nextInt(3), 0.5, 1 - r.nextInt(3)));
		}
		
		if(gamers.size() >= 5 && Game.type == GameType.CLASSIC) {
			for(int i=0;i<=500;i++) {
				Player p = gamers.get(r.nextInt(gamers.size()));
				if(!tnt.contains(p) && !gold.contains(p)) {
					p.getInventory().setItem(8, ItemNamer.gameitems(new ItemStack(Material.GOLD_BLOCK, 1)));
					p.updateInventory();
					p.sendMessage("�bYou were randomly chosen to be given the GoldProtection!");
					p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
					gold.add(p);
					setNameTag(p, ChatColor.GOLD);
					break;
				}
			}
		}
		
		if(gamers.size() >= 30 && Game.type == GameType.CLASSIC) {
			for(int i=0;i<=500;i++) {
				Player p = gamers.get(r.nextInt(gamers.size()));
				if(!tnt.contains(p) && !gold.contains(p)) {
					p.getInventory().setItem(8, ItemNamer.gameitems(new ItemStack(Material.GOLD_BLOCK, 1)));
					p.updateInventory();
					p.sendMessage("�bYou were randomly chosen to be given the GoldProtection!");
					p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
					gold.add(p);
					setNameTag(p, ChatColor.GOLD);
					break;
				}
			}
		}
		
		
		freeze = true;
		
		SignTimer.setMessage("Game started on map \"" + mapname + "\" with gametype " + type.toString().toLowerCase() + ".", "�a");
		messagePlayers("�aGame started on map \"�r" + mapname + "�a\" with gametype �r" + type.toString().toLowerCase() + "�a.");
		
		state = GameState.PROGRESS;
		countdown = 3;
		shed_id = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(TimeBomb.getInstance(), new Runnable() {

			@Override
			public void run() {
				if(countdown >= 1) {
					messagePlayers("�6�l" + countdown);
					countdown--;
					
					if(Game.gamers != null) {
						for(Player player : Game.gamers)
							player.playSound(player.getLocation(), Sound.ORB_PICKUP, 1, 0);
					}
				} else {
					messagePlayers("�6�lThe TimeBombs will explode in 60 seconds!");
					freeze = false;
					new Timer();
					new PowerupTimer();
					new EffectTimer();
					
					if(Game.gamers != null) {
						for(Player player : Game.gamers)
							player.playSound(player.getLocation(), Sound.ORB_PICKUP, 1, 1);
					}
					
					if(shed_id != null) {
						Bukkit.getServer().getScheduler().cancelTask(shed_id);
						shed_id = null;
					}
				}
			}
			
		}, 20, 20);
	}
	
	public static void checkEndGame() {
		if(Game.state == GameState.PROGRESS) {
			if(gamers.size() < 2) {
				end();
				return;
			}
			if(tnt.size() < 1) {
				end();
				return;
			}
			if(gamers.size() == gold.size()) {
				end();
				return;
			}
			if(gamers.size() == tnt.size()) {
				if(Game.type == GameType.CLASSIC)	
					end();
				else if(Game.type == GameType.INFECTED)
					endGame();
				return;
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void endGame() {
		if(Game.type == GameType.CLASSIC) {
		String bombs = null;
		for(Player p : gamers) {
			
			if(tnt.contains(p)) {
				
				plspawn.getWorld().playSound(p.getLocation(), Sound.EXPLODE, 1, 1);
				try {
					ParticleEffect.HUGE_EXPLOSION.play(p.getLocation(), 0, 0, 0, 1, 5);
				} catch (Exception e) {
					e.printStackTrace();
					plspawn.getWorld().createExplosion(p.getLocation(), 0);
				}
				
				if(bombs == null)
					bombs = p.getName();
				else
					bombs = bombs + ", " + p.getName();
				
				p.setHealth(0);
				Points.subPoints(p, "You exploded!", 2);
				MySQL.query("UPDATE `tb_PLAYERS` SET `DIED` = `DIED` + 1 WHERE `NAME` = '" + p.getName() + "';");
				continue;
			}
			
			if(gold.contains(p)) {				
				Points.addPoints(p, "You survived and you got the GoldProtection!", 3, 5);
				p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
			} else {				
				Points.addPoints(p, "You didn't explode!", 1, 2);
				p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
			}
			
			ArrayList<ItemStack> a = new ArrayList<ItemStack>();
			for(ItemStack i : p.getInventory().getContents()) {
				if(i == null)
					continue;
				if(i.getType() == Material.TNT)
					continue;
				if(i.getType() == Material.GOLD_BLOCK)
					continue;
				if(i.getType() == Material.WRITTEN_BOOK)
					continue;
				
				a.add(i);
			}
			
			if(a != null)
				ShopCommands.cart.put(p, a);
			
			for (PotionEffect effect : p.getActivePotionEffects())
		        p.removePotionEffect(effect.getType());
			
			p.getInventory().clear();
			p.setLevel(0);
			p.setExp(0);
			p.updateInventory();
			
			p.teleport(TimeBomb.lobby);
			Random r = new Random();
			p.setVelocity(new Vector(1 - r.nextInt(3), 1, 1 - r.nextInt(3)));
			
			p.setGameMode(GameMode.ADVENTURE);
		}
		
		messagePlayers("�6Game ended! �cThe bombs exploded at " + bombs + ".");
		SignTimer.setMessage("Game ended. The bombs exploded at " + bombs + ".", "�c");

		
		} else if(Game.type == GameType.INFECTED) {
			
			for(Player p : gamers) {
		
				if(gamers.size() == tnt.size()) {
					/*
					if(tnt.contains(p)) {
						Points.addPoints(p, "You won!", 3);
						p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
						Points.setArmor(p);
					} else {
						Points.subPoints(p, "You lost!", 3);
						Points.setArmor(p);
					}
					*/
				} else {
					if(tnt.contains(p)) {
						plspawn.getWorld().playSound(p.getLocation(), Sound.EXPLODE, 1, 1);
						try {
							ParticleEffect.HUGE_EXPLOSION.play(p.getLocation(), 0, 0, 0, 1, 5);
						} catch (Exception e) {
							e.printStackTrace();
							plspawn.getWorld().createExplosion(p.getLocation(), 0);
						}
						
						p.setHealth(0);
						Points.subPoints(p, "You didn't make it to infect everyone!", 3);
						MySQL.query("UPDATE `tb_PLAYERS` SET `DIED` = `DIED` + 1 WHERE `NAME` = '" + p.getName() + "';");
						continue;
					} else {
						Points.addPoints(p, "You stayed uninfected!", 5, 4);
						p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
					}
				}
				
				ArrayList<ItemStack> a = new ArrayList<ItemStack>();
				for(ItemStack i : p.getInventory().getContents()) {
					if(i == null)
						continue;
					if(i.getType() == Material.TNT)
						continue;
					if(i.getType() == Material.GOLD_BLOCK)
						continue;
					if(i.getType() == Material.WRITTEN_BOOK)
						continue;
					
					a.add(i);
				}
				
				if(a != null)
					ShopCommands.cart.put(p, a);
				
				for (PotionEffect effect : p.getActivePotionEffects())
			        p.removePotionEffect(effect.getType());
				
				p.getInventory().clear();
				p.setLevel(0);
				p.setExp(0);
				p.updateInventory();
				
				p.teleport(TimeBomb.lobby);
				Random r = new Random();
				p.setVelocity(new Vector(1 - r.nextInt(3), 1, 1 - r.nextInt(3)));
				
				p.setGameMode(GameMode.ADVENTURE);
			}
		
			messagePlayers("�6Game ended!");
			SignTimer.setMessage("Game ended.", "�c");
		}
		
		messagePlayers("�6How did you like this map? �r/tb vote");
			
		Timer.cancel();
				
		state = GameState.NONE;
		type = GameType.CLASSIC;
		tbspawn = TimeBomb.lobby;
		plspawn = TimeBomb.lobby;
		tnt.clear();
		gamers.clear();
		gold.clear();
		Freeze.clear();
		Cooldown.clear();
		new GameTimer();
		
		for(Player p: players)
			setNameTag(p, null);
		
		if(reload)
			TimeBomb.reloadTimeBomb();
	}
	
	@SuppressWarnings("deprecation")
	public static void end() {
		for(Player p : gamers) {
			ArrayList<ItemStack> a = new ArrayList<ItemStack>();
			for(ItemStack i : p.getInventory().getContents()) {
				if(i == null)
					continue;
				if(i.getType() == Material.TNT)
					continue;
				if(i.getType() == Material.GOLD_BLOCK)
					continue;
				if(i.getType() == Material.WRITTEN_BOOK)
					continue;
				
				a.add(i);
			}
			
			if(a != null)
				ShopCommands.cart.put(p, a);
			
			for (PotionEffect effect : p.getActivePotionEffects())
		        p.removePotionEffect(effect.getType());
			
			p.getInventory().clear();
			p.setLevel(0);
			p.setExp(0);
			
			p.teleport(TimeBomb.lobby);
			Random r = new Random();
			p.setVelocity(new Vector(1 - r.nextInt(3), 1, 1 - r.nextInt(3)));
			
			p.setGameMode(GameMode.ADVENTURE);
			p.updateInventory();
		}
		
		messagePlayers("�6Game ended!");
		SignTimer.setMessage("Game ended.", "�c");
		
		Timer.cancel();
		
		state = GameState.NONE;
		tbspawn = TimeBomb.lobby;
		plspawn = TimeBomb.lobby;
		tnt.clear();
		gamers.clear();
		gold.clear();
		Freeze.clear();
		Cooldown.clear();
		new GameTimer();
		
		for(Player p: players)
			setNameTag(p, null);
		
		if(reload)
			TimeBomb.reloadTimeBomb();
	}
	
	private static ConfigurationSection getZone() {
		ArrayList<String> zones = new ArrayList<String>();
		File zoneFile = new File(TimeBomb.getInstance().getDataFolder(), "zone.yml");
		FileConfiguration f = YamlConfiguration.loadConfiguration(zoneFile);

		if(nextmap != null && nextmap != "") {
			ConfigurationSection cf = f.getConfigurationSection(nextmap);
			nextmap = "";
			return cf;
		}
		
		for(String key : f.getKeys(false)) {
			zones.add(key);
		}
		if(zones.size() == 0) {
			return null;
		}
		Random r = new Random();
		ConfigurationSection cf = f.getConfigurationSection(zones.get(r.nextInt(zones.size())));
		
		while(lastmap.equalsIgnoreCase(cf.getString("name")) && zones.size() > 1)
			cf = f.getConfigurationSection(zones.get(r.nextInt(zones.size())));
		
		return cf;
	}
	
	public static void messagePlayers(String msg) {
		for(Player p : players) {
			p.sendMessage(msg);
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void join(Player p) {
		if(!Game.players.contains(p)) {
			
			if(reload) {
				p.sendMessage("�cYou can't join Timebomb right now. Reloading it after the current game ends.");
				return;
			}
			
			if(Game.state == GameState.DISABLED) {
				p.sendMessage("�cYou can't join Timebomb right now. TimeBomb is disabled.");
				return;
			}
			
			if(Game.players.size() >= 60) {
				if(Permissions.isVIP(p) || Permissions.isAdmin(p)) {
					Boolean found = false;
					for(Player player : players) {
						if(Permissions.isVIP(player) || Permissions.isAdmin(player)) {
							continue;
						} else {
							leave(player, true);
							found = true;
							break;
						}
					}
					if(!found) {
						p.sendMessage("�cNo more than 60 players can join TimeBomb. �eSorry.");
						return;
					}
				} else {
					p.sendMessage("�cOnly VIPs can join a full TimeBomb game.");
					return;
				}
			}
			
			Game.players.add(p);
			
			storedinv.put(p, p.getInventory().getContents());
			p.getInventory().clear();
			p.setExp(0);
			p.setLevel(0);
			p.setHealth(20);
			p.setFoodLevel(20);
			setNameTag(p, null);
			p.updateInventory();
			
			p.setScoreboard(board);
			
			if(Permissions.isVIP(p)) {
				FireworkEffects.spawnRandomFirework(p.getLocation());
				messagePlayers("�7+  [VIP] " + p.getName() + " joined the lobby.");
			} else {
				messagePlayers("�7+  " + p.getName() + " joined the lobby.");
			}
			
			setListName(p);
			
			p.setGameMode(GameMode.ADVENTURE);
			p.sendMessage("�aYou joined TimeBomb. Wait for the next game...");
			p.playSound(p.getLocation(), Sound.NOTE_PIANO, 1, 1);
			p.teleport(TimeBomb.lobby);
		} else {
			p.sendMessage("�aYou already joined TimeBomb. Wait for the next game...");
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void leave(Player p, Boolean force) {
		if(Game.gamers.contains(p) && !Permissions.isAdmin(p) && !force) {
			p.sendMessage("�cYou are currently in the game. You can't leave now!");
			return;
		}
		if(Game.gamers.contains(p)) {
			Game.gamers.remove(p);
			if(Game.tnt.contains(p))
				Game.tnt.remove(p);
			
			checkEndGame();
		}
		
		setNameTag(p, null);
		p.getInventory().clear();
    	p.getInventory().setHelmet(null);
    	p.getInventory().setChestplate(null);
    	p.getInventory().setLeggings(null);
    	p.getInventory().setBoots(null);
		p.setLevel(0);
		p.setExp(0);
		
		p.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
		
		p.setPlayerListName(p.getName());
		p.setGameMode(GameMode.ADVENTURE);
		
		if(Permissions.isVIP(p)) {
			messagePlayers("�7-  [VIP] " + p.getName() + " left the lobby.");
		} else {
			messagePlayers("�7-  " + p.getName() + " left the lobby.");
		}
		
		Game.players.remove(p);
		
		if(storedinv.containsKey(p))
			p.getInventory().setContents(storedinv.get(p));
				
		p.updateInventory();
		p.sendMessage("�cYou left TimeBomb. �eDo �r/tb join �eto join again.");
		p.playSound(p.getLocation(), Sound.NOTE_PIANO, 1, 1);
		p.teleport(Bukkit.getServer().getWorlds().get(0).getSpawnLocation());
	}
	
	public static void setListName(Player player) {
		String name = player.getName();
		String pListName = ChatColor.DARK_GRAY + "[TB] " + name;
		player.setPlayerListName(pListName.length() < 16 ? pListName : pListName.substring(0, 16));
	}
	
	public static void setNameTag(Player player, ChatColor color) {
		if(!player.isOnline()) {
			players.remove(player);
			return;
		}
		
		if(color == null) {
	    	player.getInventory().setHelmet(null);
	    	player.getInventory().setChestplate(null);
	    	player.getInventory().setLeggings(null);
	    	player.getInventory().setBoots(null);
	    	Points.setArmor(player);
		} else {
	    	Color acolor = null;
	    	if(color == ChatColor.RED) {
	    		acolor = Color.RED;
	    	} else if(color == ChatColor.GOLD) {
	    		acolor = Color.YELLOW;
	    	} else {
	    		acolor = Color.WHITE;
	    	}
	    	player.getInventory().setHelmet(getColoredArmor(new ItemStack(Material.LEATHER_HELMET, 1), acolor));
	    	player.getInventory().setChestplate(getColoredArmor(new ItemStack(Material.LEATHER_CHESTPLATE, 1), acolor));
	    	player.getInventory().setLeggings(getColoredArmor(new ItemStack(Material.LEATHER_LEGGINGS, 1), acolor));
	    	Points.setArmor(player);
		}
		
		TagAPI.refreshPlayer(player);
		
	    /*
	    final String oldPlayerName = player.getName();
	    final String newName;
	    
	    if(color == null)
	    	newName = ChatColor.stripColor(oldPlayerName);
	    else
	    	newName = color + oldPlayerName;
	    
	    
	    Bukkit.getServer().getScheduler().scheduleAsyncDelayedTask(TimeBomb.getInstance(), new Runnable() {
	    	public void run() {
	    			EntityPlayer ePlayer = ((CraftPlayer) player).getHandle();
	    			ePlayer.name = newName;
	    					try {
	    						for (Player p : Bukkit.getServer().getOnlinePlayers()) {
	    							if (p != player){
	    								((CraftPlayer) p).getHandle().netServerHandler.sendPacket(new Packet29DestroyEntity(player.getEntityId()));
	    								((CraftPlayer) p).getHandle().netServerHandler.sendPacket(new Packet20NamedEntitySpawn(ePlayer));
	    							}
	    						}
	    					} catch (Exception ignored) {};
	    			ePlayer.name = oldPlayerName;

	    	}
	    });
	    */
	}
	
	public static ItemStack getColoredArmor(ItemStack item, Color color) {
		LeatherArmorMeta lam = (LeatherArmorMeta) item.getItemMeta();
		lam.setColor(color);
		item.setItemMeta(lam);
		return item;
	}
	
	public static void kickall() {
		while(Game.players.size() > 0)
			Game.leave(Game.players.get(0), true);
	}
}
